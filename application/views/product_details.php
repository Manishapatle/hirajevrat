
  <?php
    $basepath = base_url()."assets";
?>



     <?php
    $basepath1 = base_url();
?>
<?php $this->load->view('include/header');?>

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url('');?>"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="<?php echo base_url().$product_details['slug'];?>">shop</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">product details</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->
      
  
        <!-- page main wrapper start -->
        <div class="shop-main-wrapper section-padding pb-0">
            <?php  
                               if($msg=$this->session->flashdata('msg'))
                               {
                              // $msg_class=$this->session->flashdata('msg_class')
                                ?> 
                             <!--   <div class="alert <?php $msg_class;?>">  -->
                                    <div class="alert alert-success"> 
                                  <strong><?php echo $msg; ?></strong>
                                  
                                </div>
                              <?php  } ?>
            <div class="container">
                <div class="row">
                    
                    <!-- product details wrapper start -->
                    <div class="col-lg-12 order-1 order-lg-2">
                        <!-- product details inner end -->
                        <div class="product-details-inner">
                            <div class="row">
                                   <div class="col-lg-5">
                                      
                                    <div class="product-large-slider">
                                         <div class="pro-large-img img-zoom" style="border: 1px solid #00000012;">
                                            <img src="<?= $product_details['image']?>" alt="product-details" />
                                        </div>
                                         <?php foreach($product_image as $image){ ?>
                                        <div class="pro-large-img img-zoom" style="border: 1px solid #00000012;">
                                            <img src="<?= $image['product_iamge']?>" alt="product-details" />
                                        </div>
                                           <?php } ?>
                                     
                                    </div>
                                   <div class="pro-nav slick-row-10 slick-arrow-style">
                                       <div class="pro-nav-thumb" style="border: 1px solid #00000012;">
                                            <img src="<?= $product_details['image']?>" alt="product-details" class="product-thumb-img">
                                          
                                        </div>
                                        <?php foreach($product_image as $image){ ?>
                                        <div class="pro-nav-thumb" style="border: 1px solid #00000012;">
                                            <img src="<?= $image['product_iamge'];?>" alt="product-details" class="product-thumb-img">
                                          
                                        </div>
                                        <?php } ?>
                                       
                                    </div>
                                </div>
                                
                                <div class="col-lg-7">
                                    <div class="product-details-des">
                                        <div class="manufacturer-name">
                                            <!--<a href="#"><?=$product_details['metal'];?></a>-->
                                        </div>
                                        <h3 class="product-name"><?=$product_details['product_name'];?></h3>
                                        <!--<div class="ratings d-flex">-->
                                        <!--    <span><i class="fa fa-star-o"></i></span>-->
                                        <!--    <span><i class="fa fa-star-o"></i></span>-->
                                        <!--    <span><i class="fa fa-star-o"></i></span>-->
                                        <!--    <span><i class="fa fa-star-o"></i></span>-->
                                        <!--    <span><i class="fa fa-star-o"></i></span>-->
                                        <!--    <div class="pro-review">-->
                                        <!--        <span>1 Reviews</span>-->
                                        <!--    </div>-->
                                        <!--</div>-->
                                        <div class="price-box">
                                            <span class="price-regular"><strong><i class="fa fa-inr"></i><?=$product_details['price'];?></strong></span>
                                            
                                        </div>
                                       
                                        <p class="pro-desc"><?= $product_details['short_description'];?></p>
                                        <div class="quantity-cart-box d-flex align-items-center">
                                            <!-- <h6 class="option-title">qty:</h6>
                                            <div class="quantity">
                                                <div class="pro-qty"><input type="text" value="1"></div>
                                            </div> -->
                                            <div class="action_link product-enquiry">
                                                <a class="btn btn-cart2" href="#" data-toggle="modal" data-target="#quick_view_enquiry">Enquiry</a>
                                            </div>
                                        </div>
                                        <!-- <div class="pro-size">
                                            <h6 class="option-title">size :</h6>
                                            <select class="nice-select">
                                                <option>S</option>
                                                <option>M</option>
                                                <option>L</option>
                                                <option>XL</option>
                                            </select>
                                        </div> -->
                                        <!-- <div class="color-option">
                                            <h6 class="option-title">color :</h6>
                                            <ul class="color-categories">
                                                <li>
                                                    <a class="c-lightblue" href="#" title="LightSteelblue"></a>
                                                </li>
                                                <li>
                                                    <a class="c-darktan" href="#" title="Darktan"></a>
                                                </li>
                                                <li>
                                                    <a class="c-grey" href="#" title="Grey"></a>
                                                </li>
                                                <li>
                                                    <a class="c-brown" href="#" title="Brown"></a>
                                                </li>
                                            </ul>
                                        </div> -->
                                        <!-- <div class="useful-links">
                                            <a href="#" data-toggle="tooltip" title="Compare"><i
                                                    class="pe-7s-refresh-2"></i>compare</a>
                                            <a href="#" data-toggle="tooltip" title="Wishlist"><i
                                                    class="pe-7s-like"></i>wishlist</a>
                                        </div> -->
                                        <!-- <div class="like-icon">
                                            <a class="facebook" href="#"><i class="fa fa-facebook"></i>like</a>
                                            <a class="twitter" href="#"><i class="fa fa-twitter"></i>tweet</a>
                                            <a class="pinterest" href="#"><i class="fa fa-pinterest"></i>save</a>
                                            <a class="google" href="#"><i class="fa fa-google-plus"></i>share</a>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- product details inner end -->

                        <!-- product details reviews start -->
                        <div class="product-details-reviews section-padding pb-0">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="product-review-info">
                                        <ul class="nav review-tab">
                                            <li>
                                                <a class="active" data-toggle="tab" href="#tab_one">description</a>
                                            </li>
                                            <li>
                                                <a data-toggle="tab" href="#tab_two">information</a>
                                            </li>
                                            <!--<li>-->
                                            <!--    <a data-toggle="tab" href="#tab_three">reviews (1)</a>-->
                                            <!--</li>-->
                                        </ul>
                                        <div class="tab-content reviews-tab">
                                            <div class="tab-pane fade show active" id="tab_one">
                                                <div class="tab-one">
                                                    <p><?=$product_details['description'];?></p>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_two">
                                                <div class="col-md-6" style="float: left;">
                                                    <table class="table table-bordered certificate-table">
                                                        <tbody>
                                                            <tr>
                                                                <td class="certificate-title"><strong>Certificate No. :</strong> </td>
                                                                <td><?=$product_details['certificate_no'];?></td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="certificate-title"><strong>Product :</strong>  </td>
                                                                <td><?=$product_details['product_name'];?></td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="certificate-title"><strong>Style No. :</strong></td>
                                                                <td><?=$product_details['style_no'];?></td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="certificate-title"><strong>Metal :</strong> </td>
                                                                <td><?=$product_details['metal'];?></td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="certificate-title"><strong>Weight :</strong> </td>
                                                                <td><?=$product_details['weight'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="certificate-title"><strong>Stamped </strong></td>
                                                                <td><?=$product_details['stamped'];?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                   
                                                </div>
                                                <div class="col-md-6" style="float: left;">
                                                    <table class="table table-bordered certificate-table">
                                                        <tbody>
                                                            <tr>
                                                                <td class="certificate-title"><strong>Conclusion :</strong> </td>
                                                                <td><?=$product_details['conclusion'];?></td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="certificate-title"><strong>Diamonds Shape : </strong>  </td>
                                                                <td><?=$product_details['shape'];?></td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="certificate-title"><strong>Color : </strong></td>
                                                                <td><?=$product_details['color'];?></td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="certificate-title"><strong>Clarity : </strong> </td>
                                                                <td><?=$product_details['clarity'];?></td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="certificate-title"><strong>Total ESt. Weight :</strong> </td>
                                                                <td><?=$product_details['total_weight'];?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="certificate-title"><strong>Comments :  </strong></td>
                                                                <td><?=$product_details['comment'];?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                       
                                                </div>
                                                <div style="clear: both;"></div>
                                            </div>
                                            <div class="tab-pane fade" id="tab_three">
                                                <form action="#" class="review-form">
                                                    <h5>1 review for <span>Chaz Kangeroo</span></h5>
                                                    <div class="total-reviews">
                                                        <div class="rev-avatar">
                                                            <img src="assets/img/about/avatar.jpg" alt="">
                                                        </div>
                                                        <div class="review-box">
                                                            <div class="ratings">
                                                                <span class="good"><i class="fa fa-star"></i></span>
                                                                <span class="good"><i class="fa fa-star"></i></span>
                                                                <span class="good"><i class="fa fa-star"></i></span>
                                                                <span class="good"><i class="fa fa-star"></i></span>
                                                                <span><i class="fa fa-star"></i></span>
                                                            </div>
                                                            <div class="post-author">
                                                                <p><span>admin -</span> 30 Mar, 2019</p>
                                                            </div>
                                                            <p>Aliquam fringilla euismod risus ac bibendum. Sed sit
                                                                amet sem varius ante feugiat lacinia. Nunc ipsum nulla,
                                                                vulputate ut venenatis vitae, malesuada ut mi. Quisque
                                                                iaculis, dui congue placerat pretium, augue erat
                                                                accumsan lacus</p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span>
                                                                Your Name</label>
                                                            <input type="text" class="form-control" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span>
                                                                Your Email</label>
                                                            <input type="email" class="form-control" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span>
                                                                Your Review</label>
                                                            <textarea class="form-control" required></textarea>
                                                            <div class="help-block pt-10"><span
                                                                    class="text-danger">Note:</span>
                                                                HTML is not translated!
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <label class="col-form-label"><span class="text-danger">*</span>
                                                                Rating</label>
                                                            &nbsp;&nbsp;&nbsp; Bad&nbsp;
                                                            <input type="radio" value="1" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="2" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="3" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="4" name="rating">
                                                            &nbsp;
                                                            <input type="radio" value="5" name="rating" checked>
                                                            &nbsp;Good
                                                        </div>
                                                    </div>
                                                    <div class="buttons">
                                                        <button class="btn btn-sqr" type="submit">Continue</button>
                                                    </div>
                                                </form> <!-- end of review-form -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- product details reviews end -->
                    </div>
                    <!-- product details wrapper end -->
                </div>
            </div>
        </div>
        <!-- page main wrapper end -->

        <!-- related products area start -->
        <section class="related-products section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!-- section title start -->
                        <div class="section-title text-center">
                            <h2 class="title">Related Products</h2>
                            <p class="sub-title">Add related products to weekly lineup</p>
                        </div>
                        <!-- section title start -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="product-carousel-4 slick-row-10 slick-arrow-style">
                            <!-- product item start -->
                            
                            <?php foreach($product_details1 as $details){ ?>
                            <div class="product-item">
                                <figure class="product-thumb">
                                    <a href="<?php echo base_url('product/').$details['slug'];?>">
                                        <img class="pri-img" src="<?= $details['image']?>" alt="product">
                                     
                                    </a>
                                    
                                    <!--<div class="button-group">-->
                                    <!--    <a href="wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to wishlist"><i class="pe-7s-like"></i></a>-->
                                    <!--    <a href="compare.html" data-toggle="tooltip" data-placement="left" title="Add to Compare"><i class="pe-7s-refresh-2"></i></a>-->
                                    <!--    <a href="#" data-toggle="modal" data-target="#quick_view"><span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="pe-7s-search"></i></span></a>-->
                                    <!--</div>-->
                                    <div class="cart-hover">
                                        <button class="btn btn-cart" data-toggle="modal" data-target="#quick_view_enquiry">Enquiry</button>
                                    </div>
                                </figure>
                                <div class="product-caption text-center">
                                    <div class="product-identity">
                                        <p class="manufacturer-name"><a href="#"><?= $details['metal']?></a></p>
                                    </div>
                                    
                                    <h6 class="product-name">
                                        <a href="#"><?= $details['product_name']?></a>
                                    </h6>
                                    <div class="price-box">
                                        <span class="price-regular"><?= $details['price']?></span>
                                      
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <!-- product item end -->
                            
                            
                          
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- related products area end -->
    </main>

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->
     
    <!-- Quick view modal start -->
    <div class="modal" id="quick_view_enquiry">
        <div class="modal-dialog modal-lg modal-dialog-centered enquiry-modal-dialog">
            <div class="modal-content">
                <!-- <img src="assets/"> -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <!-- product details inner end -->
                    <div class="product-details-inner">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="product-name" style="text-align: center;margin-bottom: 30px;">Enquiry Request</h3>
                                <div class="col-lg-12 ">
                                    <div class="contact-message">
                                        
                                        <form method="post" id="fupForm" class="contact-form enquiry-popup" action="<?php echo base_url('Home/insert_product?id='.$product_details['id']);?>">
                                            <div class="row">
                                                <?php $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
                                                <input type="hidden" name="redirect_link" value="<?=$actual_link?>">
                                                	<input type="hidden" name="product_id" id="product_id" value="<?php echo  $product_details['id'];?>">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <input name="full_name" placeholder="Name" type="text">
                                                     <span style="color: red;"><?php echo form_error('full_name'); ?></span>
                                                </div>
                                                
                                                 <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <input name="mobile_no" placeholder="Phone" type="text" >
                                                       <span style="color: red;"><?php echo form_error('mobile_no'); ?></span>
                                                </div>
                                                 
                                                
                                                <div class="col-lg-6 col-md-6 col-sm-6">

                                                    <input name="estimate_quantity" placeholder="Estimate Quantity" type="text">
                                                       <span style="color: red;"><?php echo form_error('estimate_quantity'); ?></span>
                                                </div>
                                                
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <input name="unit_type" placeholder="Unit Type" type="text">
                                                     <span style="color: red;"><?php echo form_error('unit_type'); ?></span>

                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <input name="order_value" placeholder="Approx Order Value" type="text">
                                                       <span style="color: red;"><?php echo form_error('order_value'); ?></span>
                                                </div>
                                                
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <select name="currency">
                                                        <option value="0">Select Currency</option>
                                                        <option  value="INR">INR</option>
                                                        <option  value="USD">USD</option>
                                                    </select>
                                                </div>

                                                <div class="col-lg-12 col-md-6 col-sm-6">
                                                    
                                                    <div class="col-lg-4 col-md-6 quantity-type">
                                                        <input name="reselling" type="radio" class="enquiry-radio-input" value="Reselling"> Reselling
                                                         <span style="color: red;"><?php echo form_error('reselling'); ?></span>
                                                    </div>

                                                    <div class="col-lg-4 col-md-6 quantity-type">
                                                        <input name="reselling" type="radio" class="enquiry-radio-input" value="End Use"> End Use
                                                          <span style="color: red;"><?php echo form_error('reselling'); ?></span>
                                                    </div>

                                                    <div class="col-lg-4 col-md-6 quantity-type">
                                                        <input name="reselling" type="radio" class="enquiry-radio-input" value="Raw Material"> Raw Material
                                                          <span style="color: red;"><?php echo form_error('reselling'); ?></span>
                                                    </div>

                                                </div>

                                                <div class="col-lg-12 col-md-6 col-sm-6">
                                                    <textarea name="requirement_details"  cols="30" rows="3" placeholder="Requirement Details"></textarea>
                                                      <span style="color: red;"><?php echo form_error('requirement_details'); ?></span>
                                                </div>

                                                <div class="col-12">
                                                    
                                                    <div class="contact-btn">
                                                        <button name="save" id="butsave" class="btn btn-sqr" type="submit">Send Message</button>
                                                    </div>
                                                </div>
                                               <!-- <div class="col-12 d-flex justify-content-center">
                                                    <p class="form-messege"></p>
                                                </div>-->
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- product details inner end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Quick view modal end -->

    <?php $this->load->view('include/footer');?>
     <script type="text/javascript">
  var timeout = 3000; // in miliseconds (3*1000)

$('.alert').delay(timeout).fadeOut(300);
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#butsave').on('click', function() {

    var commentid = $('#product_id').val();
    var name = $('#full_name').val();
    var email = $('#mobile_no').val();
  //  var created_time = $('#created_time').val();
   // var message = $('#message').val();
  //console.log('name');
  //console.log('email');


    if(name!="" && message!="")

    {
      $.ajax({
        url: "<?php echo base_url("Home/insert_product");?>",
        type: "POST",
        data: {
        	alert("hiiii");return false;
            commentid:commentid,
          name: name,
          email: email,
          created_time: created_time,
          message: message        
        },
        cache: false,
        success: function(dataResult){
          var dataResult = JSON.parse(dataResult);
          if(dataResult.statusCode==200){
            
            $('#fupForm').find('input:text').val('');
            $("#success").show();
            $('#success').html('Comment added successfully !');            
          }
          else if(dataResult.statusCode==201){
             alert("Error occured !");
          }
          
        }
      });
    }
    else{
      alert('Please fill all the field !');
    }
  });
});
</script>

    