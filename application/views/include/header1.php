     <?php
    $basepath = base_url()."assets";
?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Jewellery Shop </title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $basepath;?>/img/favicon.ico">

    <!-- CSS
	============================================ -->
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo $basepath;?>/css/vendor/bootstrap.min.css">
    <!-- Pe-icon-7-stroke CSS -->
    <link rel="stylesheet" href="<?php echo $basepath;?>/css/vendor/pe-icon-7-stroke.css">
    <!-- Font-awesome CSS -->
    <link rel="stylesheet" href="<?php echo $basepath;?>/css/vendor/font-awesome.min.css">
    <!-- Slick slider css -->
    <link rel="stylesheet" href="<?php echo $basepath;?>/css/plugins/slick.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="<?php echo $basepath;?>/css/plugins/animate.css">
    <!-- Nice Select css -->
    <link rel="stylesheet" href="<?php echo $basepath;?>/css/plugins/nice-select.css">
    <!-- jquery UI css -->
    <link rel="stylesheet" href="<?php echo $basepath;?>/css/plugins/jqueryui.min.css">
    <!-- main style css -->
    <link rel="stylesheet" href="<?php echo $basepath;?>/css/style.css">
    <!-- fontawesome -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.12.0/css/all.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.12.0/css/all.css">
    
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Playfair+Display&display=swap" rel="stylesheet">

</head>

<body>
    <!-- Start Header Area -->
    <header class="header-area header-wide">
        <!-- main header start -->
        <div class="main-header d-none d-lg-block">
            <!-- header top start -->
            <div class="header-top bdr-bottom">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <div class="welcome-message">
                                <p>Welcome to Chetan Prakash</p>
                            </div>
                        </div>
                        <div class="col-lg-6 text-right">
                            <div class="header-top-settings">
                                <ul class="nav align-items-center justify-content-end">
                                    <li class="curreny-wrap">
                                        <i class="fas fa-envelope header-envelop"></i> cpkankriya2008@gmail.com
                                        
                                    </li>
                                    <li class="language">
                                        <i class="fas fa-phone-volume header-envelop"></i> +91 8547632145
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header top end -->
            <!-- header middle area start -->
            <div class="header-main-area sticky">
                <div class="container">
                    <div class="row align-items-center position-relative">

                        <!-- start logo area -->
                        <div class="col-lg-2">
                            <div class="logo">
                                <a href="<?php echo base_url();?>">
                                    <img src="<?php echo $basepath;?>/img/logo/prakash-logo.png" alt="brand logo">
                                </a>
                            </div>
                        </div>
                        <!-- start logo area -->

                        <!-- main menu area start -->
                        <div class="col-lg-7 position-static">
                            <div class="main-menu-area">
                                <div class="main-menu">
                                    <!-- main menu navbar start -->
                                    <nav class="desktop-menu">
                                        <ul>
                                            <li class="active" style="margin: 0 0 0 50%;"><a href="<?php echo base_url('');?>">Home </a></li>

                                            <li class="position-static"><a href="#">Product <i class="fa fa-angle-down"></i></a>
                                                 <ul class="megamenu dropdown">
                                              <?php 	 foreach ($categories as $category)
                                                    {
                                            ?>
        									<li>
        									    	<a href="<?php echo base_url().$category->slug;?>" style="font-weight:700;"><?php echo $category->category;?></a>
        									    	
        									    	 <ul>
        									    	  
        									    	     	<?php
                                                    if(!empty($category->subs)) { ?>
                                                     
                                                     <?php   foreach ($category->subs as $sub)  { ?>
                                                          <li style="margin:0 0 0 0;"><a href="<?php echo base_url().$sub->slug;?>"><?php echo  $sub->product_name;?></a></li>
                                                    
                                                         
                                                             <?php   } ?>
                                                      <?php } ?>
                                                        </ul>
        									    	
        								
        									</li>
        										<?php } ?>
                                                     
                                                     
                                                 
                                                  
                                                  
                                                 
                                                  
                                                </ul> 
                                            </li>
                                           
                                             <li><a href="<?php echo base_url('about-us');?>">About us</a></li>
                                            <li><a href="<?php echo base_url('contact-us');?>">Contact us</a></li>
                                        </ul>
                                    </nav>
                                    <!-- main menu navbar end -->
                                </div>
                            </div>
                        </div>
                        <!-- main menu area end -->

                        <!-- mini cart area start -->
                        <div class="col-lg-3">
                            <div class="header-right d-flex align-items-center justify-content-xl-between justify-content-lg-end">
                                <div class="header-search-container">
                                    <button class="search-trigger d-xl-none d-lg-block"><i class="pe-7s-search"></i></button>
                                    <form class="header-search-box d-lg-none d-xl-block animated jackInTheBox">
                                        <input type="text" placeholder="Search entire store hire" class="header-search-field">
                                        <button class="header-search-btn"><i class="pe-7s-search"></i></button>
                                    </form>
                                </div>
                                <!-- <div class="header-configure-area">
                                    <ul class="nav justify-content-end">
                                        <li class="user-hover">
                                            <a href="#">
                                                <i class="pe-7s-user"></i>
                                            </a>
                                            <ul class="dropdown-list">
                                                <li><a href="login-register.html">login</a></li>
                                                <li><a href="login-register.html">register</a></li>
                                                <li><a href="my-account.html">my account</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="wishlist.html">
                                                <i class="pe-7s-like"></i>
                                                <div class="notification">0</div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="minicart-btn">
                                                <i class="pe-7s-shopbag"></i>
                                                <div class="notification">2</div>
                                            </a>
                                        </li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                        <!-- mini cart area end -->

                    </div>
                </div>
            </div>
            <!-- header middle area end -->
        </div>
        <!-- main header start -->

        <!-- mobile header start -->
        <!-- mobile header start -->
        <div class="mobile-header d-lg-none d-md-block sticky">
            <!--mobile header top start -->
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="mobile-main-header">
                            <div class="mobile-logo">
                                <a href="<?php echo base_url('');?>">
                                    <img src="<?php echo $basepath;?>/img/logo/logo.png" alt="Brand Logo">
                                </a>
                            </div>
                            <div class="mobile-menu-toggler">
                                <!--<div class="mini-cart-wrap">-->
                                <!--    <a href="cart.html">-->
                                <!--        <i class="pe-7s-shopbag"></i>-->
                                <!--        <div class="notification">0</div>-->
                                <!--    </a>-->
                                <!--</div>-->
                                <button class="mobile-menu-btn">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- mobile header top start -->
        </div>
        <!-- mobile header end -->
        <!-- mobile header end -->

        <!-- offcanvas mobile menu start -->
        <!-- off-canvas menu start -->
        <aside class="off-canvas-wrapper">
            <div class="off-canvas-overlay"></div>
            <div class="off-canvas-inner-content">
                <div class="btn-close-off-canvas">
                    <i class="pe-7s-close"></i>
                </div>

                <div class="off-canvas-inner">
                    <!-- search box start -->
                    <div class="search-box-offcanvas">
                        <form>
                            <input type="text" placeholder="Search Here...">
                            <button class="search-btn"><i class="pe-7s-search"></i></button>
                        </form>
                    </div>
                    <!-- search box end -->

                    <!-- mobile menu start -->
                    <div class="mobile-navigation">

                        <!-- mobile menu navigation start -->
                        <nav>
                            <ul class="mobile-menu">
                                <li class="menu-item-has-children"><a href="<?php echo base_url();?>">Home</a>
                                   
                                </li>
                                  <li><a href="<?php echo base_url('about-us');?>">About us</a></li>
                                <li><a href="<?php echo base_url('contact-us');?>">Contact us</a></li>
                                	
                                <li class="menu-item-has-children"><a href="#">Product</a>
                                    <ul class="megamenu dropdown">
                                          <?php 	 foreach ($categories as $category)
                                                    {
                                            ?>
                                        <li class="mega-title menu-item-has-children"><a href="<?php echo base_url().$category->slug;?>"><strong><?php echo $category->category;?></strong></a>
                                         <ul>
        									    	     	<?php
                                                    if(!empty($category->subs)) { ?>
                                                     
                                                     <?php   foreach ($category->subs as $sub)  { ?>
                                                          <li><a href="<?php echo base_url().$sub->slug;?>"><?php echo  $sub->product_name;?></a></li>
                                                    
                                                         
                                                             <?php   } ?>
                                                      <?php } ?>
                                                        </ul>
                                        
                                               
                                            
                                        </li>
                                        <?php } ?>
                                      
                                      
                                    </ul>
                                </li>
                             
                               
                            </ul>
                        </nav>
                        <!-- mobile menu navigation end -->
                    </div>
                    <!-- mobile menu end -->

                    

                    
                </div>
            </div>
        </aside>
        <!-- off-canvas menu end -->
        <!-- offcanvas mobile menu end -->
    </header>
    <!-- end Header Area -->