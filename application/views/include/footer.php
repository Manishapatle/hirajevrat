     <?php
    $basepath = base_url()."assets";
?>
<!-- footer area start -->
    <footer class="footer-widget-area">
        <div class="footer-top section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-item">
                            <div class="widget-title">
                                <!--<div class="widget-logo">
                                    <a href="#">
                                        <img src="<?php //echo $basepath;?>/img/logo/logo.png" alt="brand logo">
                                    </a>
                                </div>-->
                            </div>
                            <div class="widget-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-item">
                            <h6 class="widget-title">Contact Us</h6>
                            <div class="widget-body">
                                <address class="contact-block">
                                    <ul>
                                        <li><i class="pe-7s-home"></i> 120 A/B, First Floor Ratan Market, Tanga Stand, Sarafa Bazar, Itwari, Nagpur, Maharashtra - 440002, India</li>
                                        <li><i class="pe-7s-mail"></i> <a href="mailto:cpkankriya2008@gmail.com">cpkankriya2008@gmail.com</a></li>
                                        <li><i class="pe-7s-call"></i> <a href="tel:8888912170">+91-8888912170</a></li>
                                    </ul>
                                </address>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-item">
                            <h6 class="widget-title">Information</h6>
                            <div class="widget-body">
                                <ul class="info-list">
                                    <li><a href="#">about us</a></li>
                                    <li><a href="#">Delivery Information</a></li>
                                    <li><a href="#">privet policy</a></li>
                                    <li><a href="#">Terms & Conditions</a></li>
                                    <li><a href="#">contact us</a></li>
                                    <li><a href="#">site map</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-item">
                            <h6 class="widget-title">Follow Us</h6>
                            <div class="widget-body social-link">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                                <a href="#"><i class="fab fa-twitter"></i></a>
                                <a href="#"><i class="fab fa-instagram"></i></a>
                                <a href="#"><i class="fab fa-youtube"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
       <!-- <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="copyright-text text-center">
                            <p>Powered By <a href="#">Jewellery</a>. Store  © 2020</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
    </footer>
    <!-- footer area end -->


    <!-- offcanvas mini cart start -->
    <div class="offcanvas-minicart-wrapper">
        <div class="minicart-inner">
            <div class="offcanvas-overlay"></div>
            <div class="minicart-inner-content">
                <div class="minicart-close">
                    <i class="pe-7s-close"></i>
                </div>
                <div class="minicart-content-box">
                    <div class="minicart-item-wrapper">
                        <ul>
                            <li class="minicart-item">
                                <div class="minicart-thumb">
                                    <a href="#">
                                        <img src="<?php echo $basepath;?>/img/cart/cart-1.jpg" alt="product">
                                    </a>
                                </div>
                                <div class="minicart-content">
                                    <h3 class="product-name">
                                        <a href="#">Dozen White Botanical Linen Dinner Napkins</a>
                                    </h3>
                                    <p>
                                        <span class="cart-quantity">1 <strong>&times;</strong></span>
                                        <span class="cart-price">$100.00</span>
                                    </p>
                                </div>
                                <button class="minicart-remove"><i class="pe-7s-close"></i></button>
                            </li>
                            <li class="minicart-item">
                                <div class="minicart-thumb">
                                    <a href="#">
                                        <img src="<?php echo $basepath;?>/img/cart/cart-2.jpg" alt="product">
                                    </a>
                                </div>
                                <div class="minicart-content">
                                    <h3 class="product-name">
                                        <a href="#">Dozen White Botanical Linen Dinner Napkins</a>
                                    </h3>
                                    <p>
                                        <span class="cart-quantity">1 <strong>&times;</strong></span>
                                        <span class="cart-price">$80.00</span>
                                    </p>
                                </div>
                                <button class="minicart-remove"><i class="pe-7s-close"></i></button>
                            </li>
                        </ul>
                    </div>

                    <div class="minicart-pricing-box">
                        <ul>
                            <li>
                                <span>sub-total</span>
                                <span><strong>$300.00</strong></span>
                            </li>
                            <li>
                                <span>Eco Tax (-2.00)</span>
                                <span><strong>$10.00</strong></span>
                            </li>
                            <li>
                                <span>VAT (20%)</span>
                                <span><strong>$60.00</strong></span>
                            </li>
                            <li class="total">
                                <span>total</span>
                                <span><strong>$370.00</strong></span>
                            </li>
                        </ul>
                    </div>

                    <div class="minicart-button">
                        <a href="#"><i class="fa fa-shopping-cart"></i> View Cart</a>
                        <a href="#"><i class="fa fa-share"></i> Checkout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- offcanvas mini cart end -->

    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="<?php echo $basepath;?>/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- jQuery JS -->
    <script src="<?php echo $basepath;?>/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="<?php echo $basepath;?>/js/vendor/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="<?php echo $basepath;?>/js/vendor/bootstrap.min.js"></script>
    <!-- slick Slider JS -->
    <script src="<?php echo $basepath;?>/js/plugins/slick.min.js"></script>
    <!-- Countdown JS -->
    <script src="<?php echo $basepath;?>/js/plugins/countdown.min.js"></script>
    <!-- Nice Select JS -->
    <script src="<?php echo $basepath;?>/js/plugins/nice-select.min.js"></script>
    <!-- jquery UI JS -->
    <script src="<?php echo $basepath;?>/js/plugins/jqueryui.min.js"></script>
    <!-- Image zoom JS -->
    <script src="<?php echo $basepath;?>/js/plugins/image-zoom.min.js"></script>
    <!-- Imagesloaded JS -->
    <script src="<?php echo $basepath;?>/js/plugins/imagesloaded.pkgd.min.js"></script>
    <!-- Instagram feed JS -->
    <script src="<?php echo $basepath;?>/js/plugins/instagramfeed.min.js"></script>
    <!-- mailchimp active js -->
    <script src="<?php echo $basepath;?>/js/plugins/ajaxchimp.js"></script>
    <!-- contact form dynamic js -->
    <script src="<?php echo $basepath;?>/js/plugins/ajax-mail.js"></script>
    <!-- google map api -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfmCVTjRI007pC1Yk2o2d_EhgkjTsFVN8"></script>
    <!-- google map active js -->
    <script src="<?php echo $basepath;?>/js/plugins/google-map.js"></script>
    <!-- Main JS -->
    <script src="<?php echo $basepath;?>/js/main.js"></script>
    
   
</body>


<!-- Mirrored from demo.hasthemes.com/corano-preview/corano/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Dec 2020 05:01:41 GMT -->
</html>