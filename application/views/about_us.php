    <?php
    $basepath = base_url()."assets";
?>



     <?php
    $basepath1 = base_url();
?>
<?php $this->load->view('include/header');?>


    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url('');?>"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">About us</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- about us area start -->
        <section class="about-us section-padding">
            <div class="container">
                <div class="row align-items-center">
                    <!--<div class="col-lg-5">-->
                    <!--    <div class="about-thumb">-->
                    <!--        <img src="<?php echo $basepath;?>/img/about/about.jpg" alt="about thumb">-->
                    <!--    </div>-->
                    <!--</div>-->
                    <div class="col-lg-12">
                        <div class="about-content">
                            <h2 class="about-title">About Us</h2>
                            <p>Our company, <strong>CHETAN PRAKASH REAL DIAMOND JEWELLERY</strong> was established in the year 2006 under the blissful guidance of <strong>Mr. Prakashchandji M. Kankriya.</strong> Our head office is situated at Itwari Sarafa market, Nagpur. Our firm works in Nagpur as a wholesale distributor of gems & diamond jewellery and sells the product in Maharashtra, Chhatisgarh and Madhya Pradesh. Basically, we are dealing in jewellery since 1930 and want to establish the fame and name of our company all over India in diamond jewellery and gems.</p>
                            <h5 class="about-sub-title">Resources :</h5>
                            <p>The company is backed with state-of-the-art workshop spread over the sprawling area of 1000sq.ft, which is equipped with all the advanced equipments required for polishing, grinding, plating, etc. of our products. All the work is done by the team of experienced artisans and they bring forth innovative and mesmerizing designs depending on clients requirements.</p>
                            <h5 class="about-sub-title">Quality Assurance :</h5>
                            <p>We are intensely committed to deliver the best quality, competitive products, services & solutions to perfectly meet the requirements of the customer, both internal & external, on time, all the time. We also give certification of the particular gems to the customers as per their requirement. The certifications may be from Gemmological Institute of India (GII) or India Diamond Institute {IDI).</p>
                            <h5 class="about-sub-title">Vision & Mission :</h5>
                            <p>Our vision is to create something more precious than business along with business, that is relationship with the esteemed patrons by serving them in a best feasible manner.</p>
                            <p>Our mission is to become a synonymous brand for specific niche of most common individuals in the industry of studded jewellery by creating a unique distribution Channel which would create synergy for both customer and us in terms of cost cutting to provide goods at most affordable rates.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- about us area end -->

        
    </main>

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->
<?php $this->load->view('include/footer');?>
   