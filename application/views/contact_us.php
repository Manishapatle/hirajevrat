
     <?php
    $basepath = base_url()."assets";
?>


<?php $this->load->view('include/header');?>
    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">contact us</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- google map start -->
      <!--   <div class="map-area section-padding">
            <div id="google-map"></div>
        </div> -->
        <!-- google map end -->

        <!-- contact area start -->
        <div class="contact-area section-padding pt-20">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="contact-info">
                            <h4 class="contact-title">Contact Us</h4>
                            <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum
                                est notare quam littera gothica, quam nunc putamus parum claram anteposuerit litterarum
                                formas human.</p>
                            <ul>
                                <li><i class="fa fa-fax"></i> Address : 120 A/B, First Floor Ratan Market, Tanga Stand, Sarafa Bazar, Itwari, Nagpur, Maharashtra - 440002, India</li>
                                <li><i class="fa fa-envelope"></i>  E-mail: cpkankriya2008@gmail.com</li>
                                <li><i class="fa fa-phone"></i>+91-8888912170, +91-7276869001</li>
                            </ul>
                            
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="contact-message">
                            <h4 class="contact-title">Tell Us Your Project</h4>
                            	<?php  
						                   if($msg=$this->session->flashdata('msg'))
						                      {
						                         // $msg_class=$this->session->flashdata('msg_class')
						                 ?> 
						                    <!--   <div class="alert <?php $msg_class;?>">  -->
						                         <div class="alert alert-success"> 
						                    <strong><?php echo $msg; ?></strong>
						      
						                     </div>
						                 <?php  } ?>
                          		<form method="post" action="<?php echo base_url().'Home/send_mail'; ?>">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="name" placeholder="Name *" type="text" required>
                                        	<?php echo form_error('name'); ?> 
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="phone" placeholder="Phone *" type="text" required>
                                        	<?php echo form_error('phone'); ?> 
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="email" placeholder="Email *" type="text" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <input name="subject" placeholder="Subject *" type="text">
                                    </div>
                                    <div class="col-12">
                                        <div class="contact2-textarea text-center">
                                            <textarea placeholder="Message *" name="message" class="form-control2" required=""></textarea>
                                        </div>
                                        <div class="contact-btn">
                                            <button class="btn btn-sqr" type="submit">Send Message</button>
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex justify-content-center">
                                        <p class="form-messege"></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- contact area end -->
    </main>

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->
     <?php $this->load->view('include/footer');?>
 <script type="text/javascript">
  var timeout = 3000; // in miliseconds (3*1000)

$('.alert').delay(timeout).fadeOut(300);
</script>
    