
  <?php
    $basepath = base_url()."assets";
?>


<?php $this->load->view('include/header');?>

    <main>
        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?php echo base_url('');?>"><i class="fa fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">shop</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- page main wrapper start -->
        <div class="shop-main-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <!-- sidebar area start -->
                    <div class="col-lg-3 order-2 order-lg-1">
                        <aside class="sidebar-wrapper">
                            <!-- single sidebar start -->
                            <div class="sidebar-single">
                                <h5 class="sidebar-title">categories</h5>
                                
                                 <div class="sidebar-body">
                                    
                                    <ul class="shop-categories">
                                         <?php 	 foreach ($categories as $category)
                                                    {
                                            ?>
                                        <li><a href="<?php echo base_url().$category->slug;?>"><span><?=$category->category ?></span></a></li>
                                         <?php } ?>
                                    </ul>
                                  
                                </div>
                                
                             
                            </div>
                            <!-- single sidebar end -->

                            

                            

                            

                            

                            <!-- single sidebar start -->
                            <div class="sidebar-banner">
                                <div class="img-container">
                                    <a href="#">
                                        <img src="assets/img/banner/sidebar-banner.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <!-- single sidebar end -->
                        </aside>
                    </div>
                    <!-- sidebar area end -->

                    <!-- shop main wrapper start -->
                    <div class="col-lg-9 order-1 order-lg-2">
                        <div class="shop-product-wrapper">
                            <!-- shop product top wrap start -->
                          
                            <!-- shop product top wrap start -->

                            <!-- product item list wrapper start -->
                            <div class="shop-product-wrap grid-view row mbn-30">
                                <!-- product single item start -->
                                 <?php foreach($product_details as $details){ ?>
                                <div class="col-md-4 col-sm-6">
                                    <!-- product grid start -->
                                    
                                   
                                    <div class="product-item">
                                        <figure class="product-thumb">
                                            <a href="<?php echo base_url('product/').$details['slug'];?>">
                                                <img class="pri-img" src="<?= $details['image']?>" alt="product">
                                                <img class="sec-img" src="<?= $details['image']?>" alt="product">
                                            </a>
                                            <div class="product-badge">
                                               <!-- <div class="product-label new">
                                                    <span>new</span>
                                                </div>
                                                <div class="product-label discount">
                                                    <span>10%</span>
                                                </div>-->
                                            </div>
                                            <!--<div class="button-group">-->
                                            <!--    <a href="wishlist.html" data-toggle="tooltip" data-placement="left" title="Add to wishlist"><i class="pe-7s-like"></i></a>-->
                                            <!--    <a href="compare.html" data-toggle="tooltip" data-placement="left" title="Add to Compare"><i class="pe-7s-refresh-2"></i></a>-->
                                            <!--    <a href="#" data-toggle="modal" data-target="#quick_view"><span data-toggle="tooltip" data-placement="left" title="Quick View"><i class="pe-7s-search"></i></span></a>-->
                                            <!--</div>-->
                                          <!--  <div class="cart-hover">
                                                <button class="btn btn-cart">add to cart</button>
                                            </div>-->
                                        </figure>
                                        <div class="product-caption text-center">
                                            <div class="product-identity">
                                                <p class="manufacturer-name"><a href="#"><?= $details['metal']?></a></p>
                                            </div>
                                            
                                            <h6 class="product-name">
                                                <a href="#"><?= $details['product_name']?></a>
                                            </h6>
                                            <div class="price-box">
                                                <span class="price-regular"><?= $details['price']?></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!-- product grid end -->
                                 
                                 
                                </div>
                                   <?php } ?>
                                <!-- product single item start -->

                            </div>
                            <!-- product item list wrapper end -->

                            <!-- start pagination area -->
                            <div class="paginatoin-area text-center">
                                <ul class="pagination-box">
                                    <li><a class="previous" href="#"><i class="pe-7s-angle-left"></i></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a class="next" href="#"><i class="pe-7s-angle-right"></i></a></li>
                                </ul>
                            </div>
                            <!-- end pagination area -->
                        </div>
                    </div>
                    <!-- shop main wrapper end -->
                </div>
            </div>
        </div>
        <!-- page main wrapper end -->
    </main>

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->
     <?php $this->load->view('include/footer');?>
     
  