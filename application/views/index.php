
     <?php
    $basepath = base_url()."assets";
?>



     <?php
    $basepath1 = base_url();
?>
<?php $this->load->view('include/header');?>

    <main>
        <!-- hero slider area start -->
        <section class="slider-area">
            <div class="hero-slider-active slick-arrow-style slick-arrow-style_hero slick-dot-style">
                <!-- single slider item start -->
                
                 <?php foreach($slider as $slide){
                    
                 ?>
                <div class="hero-single-slide hero-overlay">
                    <div class="hero-slider-item bg-img" data-bg="admin/<?=$slide['image'];?>">
                    
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <!--<div class="hero-slider-content slide-1">-->
                                    <!--    <h2 class="slide-title">Flower Diamond<span>Collection</span></h2>-->
                                    <!--    <h4 class="slide-desc">Budget Jewellery Starting At ₹295.99</h4>-->
                                    <!--    <a href="#" class="btn btn-hero">Read More</a>-->
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                
             
            </div>
        </section>
        <!-- hero slider area end -->

        <!-- service policy area start -->
        <div class="service-policy">
            <div class="container">
                  <?php  
                               if($msg=$this->session->flashdata('msg'))
                               {
                              // $msg_class=$this->session->flashdata('msg_class')
                                ?> 
                             <!--   <div class="alert <?php $msg_class;?>">  -->
                                    <div class="alert alert-success"> 
                                  <strong><?php echo $msg; ?></strong>
                                  
                                </div>
                              <?php  } ?>
                <div class="policy-block section-padding">
                    <div class="col-md-12 home-about">
                        <p>Diamond Jewelry is one of those precious things that a woman wishes to have in her life. CHETAN PRAKASH REAL DIAMOND JEWELLERY is one place where you can cherish all your desires of elegantly designed diamond jewelry. Wholesaler and Supplier of Designer Real Diamond Jewellery. From our extensive range of <strong>Diamond Jewellery in India</strong> We acquaint ourselves as a prestigious manufacturer and supplier of all types of Diamond Rings, Diamond Pendants, Diamond Necklace, etc. We create jewellery of unique & innovative designs, which are highly admired by all our clients. We excel in quality when it comes to making embellishments in yellow gold and turn them into a magnificent range of diamond jewelry. Our competence to offer certified jewelry with complete quality assurance makes us a reliable business entity. Our motive is to satisfy all the needs of the customers in stipulated time. We always endeavor to provide our customers the superior, unmatched and best quality jewelry. We strive to maintain a never ending relationship with our honored clients via our superior quality products.</p>
                    </div>
                    <div class="row mtn-30">
                        <div class="col-sm-6 col-lg-3">
                            <div class="policy-item">
                                <div class="policy-icon">
                                    <i class="pe-7s-plane"></i>
                                </div>
                                <div class="policy-content">
                                    <h6>Free Shipping</h6>
                                    <p>Free shipping all order</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="policy-item">
                                <div class="policy-icon">
                                    <i class="pe-7s-help2"></i>
                                </div>
                                <div class="policy-content">
                                    <h6>Support 24/7</h6>
                                    <p>Support 24 hours a day</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="policy-item">
                                <div class="policy-icon">
                                    <i class="pe-7s-back"></i>
                                </div>
                                <div class="policy-content">
                                    <h6>Money Return</h6>
                                    <p>30 days for free return</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="policy-item">
                                <div class="policy-icon">
                                    <i class="pe-7s-credit"></i>
                                </div>
                                <div class="policy-content">
                                    <h6>100% Payment Secure</h6>
                                    <p>We ensure secure payment</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- service policy area end -->
        
        
        <!-- banner statistics area start -->
        <div class="banner-statistics-area">
            <div class="container">
                <div class="row row-20 mtn-20">
                    <?php foreach($category_feature as $feature){?>
                    <div class="col-sm-6">
                        <figure class="banner-statistics mt-20">
                            <a href="<?php echo base_url().$feature['slug'];?>">
                                <img src="<?= $feature['feature_image'] ?>" alt="product banner" class="four-category">
                            </a>
                            <div class="banner-content text-right">
                                <h5 class="banner-text1">BEAUTIFUL</h5>
                                <h2 class="banner-text2"><span><?= $feature['category'] ?></span></h2>
                               
                            </div>
                        </figure>
                    </div>
                    <?php } ?>
                   
                   
                  
                </div>
            </div>
        </div>
        <!-- banner statistics area end -->
        
           <!-- product area start -->
        <section class="product-area section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!-- section title start -->
                        <div class="section-title text-center">
                            <h2 class="title">our products</h2>
                            <p class="sub-title">Add our products to weekly lineup</p>
                        </div>
                        <!-- section title start -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="product-container">
                            <!-- product tab menu start -->
                            
                            
                            <div class="product-tab-menu">
                                <ul class="nav justify-content-center">
                                     <?php $i=1; 	 foreach ($categories as $k=> $category){?>
                                        <li><a href="#tab<?=$k?>" <?php if($i==1){ ?> class="active" <?php } ?> data-toggle="tab"><?= $category->category?></a></li>
                                    <?php $i++; } ?>
                                    
                                    
                                </ul>
                            </div>
                            <!-- product tab menu end -->

                            <!-- product tab content start -->
                            <div class="tab-content">
                                <?php $z=1; 	
                                foreach ($categories as $k1=> $category){ ?>
                                    
                                    <div class="tab-pane fade <?php if($z==1){ ?> show active <?php } ?>" id="tab<?=$k1?>">
                                        <div class="product-carousel-4 slick-row-10 slick-arrow-style">
                                        
                                       <?php  foreach ($category->subs as $j=> $cat){?>
                                            <!-- product item start -->
                                            <div class="product-item our-product-item">
                                                <figure class="product-thumb">
                                                    <a href="<?php echo base_url('product/').$cat->slug;?>">
                                                    
                                                        <img class="pri-img" src="<?= $cat->image?>" alt="product">
                                                        <img class="sec-img" src="<?= $cat->image?>" alt="product">
                                                    </a>
                                                    
                                                   
                                                    <div class="cart-hover">
                                                        <button class="btn btn-cart"  onclick="add_product_id('<?=$cat->id?>');">Enquiry</button>
                                                    </div>
                                                </figure>
                                                <div class="product-caption text-center product-item-head">
                                                    <div class="product-identity">
                                                        <p class="manufacturer-name"><a href="<?php echo base_url('product/').$cat->slug;?>"><?= $cat->metal?></a></p>
                                                    </div>
                                                    
                                                    <h6 class="product-name">
                                                        <a href="<?php echo base_url('product/').$cat->slug;?>"><?= $cat->product_name?></a>
                                                    </h6>
                                                    <div class="price-box">
                                                        <span class="price-regular">₹ <?= $cat->price?></span>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- product item end -->
                                        <?php } ?>
                                            
                                        </div>
                                    </div>
                                   
                                    
                               <?php $z++;  } ?>
                            </div>
                            <!-- product tab content end -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- product area end -->

    

        
        <!-- product banner statistics area start -->
        <section class="product-banner-statistics">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="product-banner-carousel slick-row-10">
                            <!-- banner single slide start -->
                            <?php foreach($category1 as $cat1){?>
                            <div class="banner-slide-item">
                                <figure class="banner-statistics">
                                    <a href="<?php echo base_url().$cat1['slug'];?>">
                                        <img src="<?= $cat1['image']?>" alt="product banner" class="feature-image">
                                    </a>
                                    <div class="banner-content banner-content_style2">
                                        <h5 class="banner-text3"><a href="<?php echo base_url().$cat1['slug'];?>"><?= $cat1['category']?></a></h5>
                                    </div>
                                </figure>
                            </div>
                            <?php } ?>
                            <!-- banner single slide start -->
                            <!-- banner single slide start -->
                          
                           
                         
                          
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- product banner statistics area end -->

        <!-- featured product area start -->
        <section class="feature-product section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!-- section title start -->
                        <div class="section-title text-center">
                            <h2 class="title">featured products</h2>
                            <p class="sub-title">Add featured products to weekly lineup</p>
                        </div>
                        <!-- section title start -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="product-carousel-4_2 slick-row-10 slick-arrow-style">
                            <!-- product item start -->
                          <?php foreach($product_feature as $feature){?>
                                        <div class="product-item our-product-item">
                                            <figure class="product-thumb">
                                              
                                                <a href="<?php echo base_url('product/').$feature['slug'];?>">
                                                    <img class="pri-img" src="<?=$feature['image']?>" alt="product" >
                                                    <img class="sec-img" src="<?=$feature['image']?>" alt="product" >
                                                </a>
                                                
                                             
                                                <div class="cart-hover">
                                                    <button class="btn btn-cart" onclick="add_product_id('<?=$feature['id']?>');">Enquiry</button>
                                                </div>
                                            </figure>
                                            <div class="product-caption text-center product-item-head">
                                                <div class="product-identity">
                                                    <p class="manufacturer-name"><a href="<?php echo base_url('product/').$feature['slug'];?>"></a></p>
                                                </div>
                                                
                                                <h6 class="product-name">
                                                    <a href="<?php echo base_url('product/').$feature['slug'];?>"><?=$feature['product_name']?></a>
                                                </h6>
                                                <div class="price-box">
                                                    <span class="price-regular">₹ <?=$feature['price']?></span>
                                                 
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                            <!-- product item end -->


                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </section>
        <!-- featured product area end -->
        

   <!-- testimonial area start -->
        <section class="testimonial-area section-padding bg-img" data-bg="<?php echo $basepath;?>/img/testimonial/testimonials-bg.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!-- section title start -->
                        <div class="section-title text-center">
                            <h2 class="title">testimonials</h2>
                            <p class="sub-title">What they say</p>
                        </div>
                        <!-- section title start -->
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-12">
                        <div class="testimonial-thumb-wrapper">
                            <div class="testimonial-thumb-carousel">
                                <?php foreach($testimonial as $test){ ?>
                                <div class="testimonial-thumb">
                                    <img src="admin/<?= $test['image']?>" alt="testimonial-thumb">
                                </div>
                                <?php } ?>
                               
                            </div>
                        </div>
                        <div class="testimonial-content-wrapper">
                            <div class="testimonial-content-carousel">
                                  <?php foreach($testimonial as $test){ ?>
                                <div class="testimonial-content">
                                    <p><?= $test['description'] ?></p>
                                   
                                    <h5 class="testimonial-author"><?= $test['title'] ?></h5>
                                </div>
                                <?php } ?>
                               
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- testimonial area end -->


        

        <!-- latest blog area start -->
        <section class="latest-blog-area section-padding pt-50">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!-- section title start -->
                        <div class="section-title text-center">
                            <h2 class="title">latest blogs</h2>
                            <p class="sub-title">There are latest blog posts</p>
                        </div>
                        <!-- section title start -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="blog-carousel-active slick-row-10 slick-arrow-style">
                            <!-- blog post item start -->
                            <div class="blog-post-item">
                                <figure class="blog-thumb">
                                    <a href="blog-details.html">
                                        <img src="assets/img/blog/blog-img1.jpg" alt="blog image">
                                    </a>
                                </figure>
                                <div class="blog-content">
                                    <div class="blog-meta">
                                        <p>25/03/2019 | <a href="#">Corano</a></p>
                                    </div>
                                    <h5 class="blog-title">
                                        <a href="blog-details.html">Celebrity Daughter Opens Up About Having Her Eye Color Changed</a>
                                    </h5>
                                </div>
                            </div>
                            <!-- blog post item end -->

                            <!-- blog post item start -->
                            <div class="blog-post-item">
                                <figure class="blog-thumb">
                                    <a href="blog-details.html">
                                        <img src="assets/img/blog/blog-img2.jpg" alt="blog image">
                                    </a>
                                </figure>
                                <div class="blog-content">
                                    <div class="blog-meta">
                                        <p>25/03/2019 | <a href="#">Corano</a></p>
                                    </div>
                                    <h5 class="blog-title">
                                        <a href="blog-details.html">Children Left Home Alone For 4 Days In TV series Experiment</a>
                                    </h5>
                                </div>
                            </div>
                            <!-- blog post item end -->

                            <!-- blog post item start -->
                            <div class="blog-post-item">
                                <figure class="blog-thumb">
                                    <a href="blog-details.html">
                                        <img src="assets/img/blog/blog-img3.jpg" alt="blog image">
                                    </a>
                                </figure>
                                <div class="blog-content">
                                    <div class="blog-meta">
                                        <p>25/03/2019 | <a href="#">Corano</a></p>
                                    </div>
                                    <h5 class="blog-title">
                                        <a href="blog-details.html">Lotto Winner Offering Up Money To Any Man That Will Date Her</a>
                                    </h5>
                                </div>
                            </div>
                            <!-- blog post item end -->

                            <!-- blog post item start -->
                            <div class="blog-post-item">
                                <figure class="blog-thumb">
                                    <a href="blog-details.html">
                                        <img src="assets/img/blog/blog-img4.jpg" alt="blog image">
                                    </a>
                                </figure>
                                <div class="blog-content">
                                    <div class="blog-meta">
                                        <p>25/03/2019 | <a href="#">Corano</a></p>
                                    </div>
                                    <h5 class="blog-title">
                                        <a href="blog-details.html">People are Willing Lie When Comes Money, According to Research</a>
                                    </h5>
                                </div>
                            </div>
                            <!-- blog post item end -->

                            <!-- blog post item start -->
                            <div class="blog-post-item">
                                <figure class="blog-thumb">
                                    <a href="blog-details.html">
                                        <img src="assets/img/blog/blog-img5.jpg" alt="blog image">
                                    </a>
                                </figure>
                                <div class="blog-content">
                                    <div class="blog-meta">
                                        <p>25/03/2019 | <a href="#">Corano</a></p>
                                    </div>
                                    <h5 class="blog-title">
                                        <a href="blog-details.html">romantic Love Stories Of Hollywood’s Biggest Celebrities</a>
                                    </h5>
                                </div>
                            </div>
                            <!-- blog post item end -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- latest blog area end -->

        <!-- brand logo area start -->
        <div class="brand-logo section-padding pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="brand-logo-carousel slick-row-10 slick-arrow-style">
                            <!-- single brand start -->
                            <div class="brand-item">
                                <a href="#">
                                    <img src="assets/img/brand/1.png" alt="">
                                </a>
                            </div>
                            <!-- single brand end -->

                            <!-- single brand start -->
                            <div class="brand-item">
                                <a href="#">
                                    <img src="<?php echo $basepath;?>/img/brand/2.png" alt="">
                                </a>
                            </div>
                            <!-- single brand end -->

                            <!-- single brand start -->
                            <div class="brand-item">
                                <a href="#">
                                    <img src="<?php echo $basepath;?>/img/brand/3.png" alt="">
                                </a>
                            </div>
                            <!-- single brand end -->

                            <!-- single brand start -->
                            <div class="brand-item">
                                <a href="#">
                                    <img src="<?php echo $basepath;?>/img/brand/4.png" alt="">
                                </a>
                            </div>
                            <!-- single brand end -->

                            <!-- single brand start -->
                            <div class="brand-item">
                                <a href="#">
                                    <img src="<?php echo $basepath;?>/img/brand/5.png" alt="">
                                </a>
                            </div>
                            <!-- single brand end -->

                            <!-- single brand start -->
                            <div class="brand-item">
                                <a href="#">
                                    <img src="<?php echo $basepath;?>/img/brand/6.png" alt="">
                                </a>
                            </div>
                            <!-- single brand end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- brand logo area end -->
    </main>
    
     <!-- Quick view modal start -->
    <div class="modal" id="quick_view">
        <div class="modal-dialog modal-lg modal-dialog-centered enquiry-modal-dialog">
            <div class="modal-content">
                <!-- <img src="assets/"> -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <!-- product details inner end -->
                    <div class="product-details-inner">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="product-name" style="text-align: center;margin-bottom: 30px;">Enquiry Request</h3>
                                <div class="col-lg-12 ">
                                    <div class="contact-message">
                                        
                                        <form method="post" id="fupForm" class="contact-form enquiry-popup" action="<?php echo base_url('Home/insert_product?id='.$feature['id']);?>">
                                            <div class="row">
                                                  <?php $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
                                                <input type="hidden" name="redirect_link" value="<?=$actual_link?>">
                                                	<input type="hidden" name="product_id" id="product_id" value="<?php echo  $feature['id'];?>">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <input name="full_name" placeholder="Name" type="text">
                                                     <span style="color: red;"><?php echo form_error('full_name'); ?></span>
                                                </div>
                                                
                                                 <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <input name="mobile_no" placeholder="Phone" type="text" >
                                                       <span style="color: red;"><?php echo form_error('mobile_no'); ?></span>
                                                </div>
                                                 
                                                
                                                <div class="col-lg-6 col-md-6 col-sm-6">

                                                    <input name="estimate_quantity" placeholder="Estimate Quantity" type="text">
                                                       <span style="color: red;"><?php echo form_error('estimate_quantity'); ?></span>
                                                </div>
                                                
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <input name="unit_type" placeholder="Unit Type" type="text">
                                                     <span style="color: red;"><?php echo form_error('unit_type'); ?></span>

                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <input name="order_value" placeholder="Approx Order Value" type="text">
                                                       <span style="color: red;"><?php echo form_error('order_value'); ?></span>
                                                </div>
                                                
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <select name="currency">
                                                        <option value="0">Select Currency</option>
                                                        <option  value="INR">INR</option>
                                                        <option  value="USD">USD</option>
                                                    </select>
                                                </div>

                                                <div class="col-lg-12 col-md-6 col-sm-6">
                                                    
                                                    <div class="col-lg-4 col-md-6 quantity-type">
                                                        <input name="reselling" type="radio" class="enquiry-radio-input" value="Reselling"> Reselling
                                                         <span style="color: red;"><?php echo form_error('reselling'); ?></span>
                                                    </div>

                                                    <div class="col-lg-4 col-md-6 quantity-type">
                                                        <input name="reselling" type="radio" class="enquiry-radio-input" value="End Use"> End Use
                                                          <span style="color: red;"><?php echo form_error('reselling'); ?></span>
                                                    </div>

                                                    <div class="col-lg-4 col-md-6 quantity-type">
                                                        <input name="reselling" type="radio" class="enquiry-radio-input" value="Raw Material"> Raw Material
                                                          <span style="color: red;"><?php echo form_error('reselling'); ?></span>
                                                    </div>

                                                </div>

                                                <div class="col-lg-12 col-md-6 col-sm-6">
                                                    <textarea name="requirement_details"  cols="30" rows="3" placeholder="Requirement Details"></textarea>
                                                      <span style="color: red;"><?php echo form_error('requirement_details'); ?></span>
                                                </div>

                                                <div class="col-12">
                                                    
                                                    <div class="contact-btn">
                                                        <button name="save" id="butsave" class="btn btn-sqr" type="submit">Send Message</button>
                                                    </div>
                                                </div>
                                               <!-- <div class="col-12 d-flex justify-content-center">
                                                    <p class="form-messege"></p>
                                                </div>-->
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- product details inner end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Quick view modal end -->

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->
    <?php $this->load->view('include/footer');?>
<script>
    function add_product_id(p_id){
        
        $('#product_id').val(p_id);
        $('#quick_view').modal('show');
    }
</script>
  <script type="text/javascript">
  var timeout = 3000; // in miliseconds (3*1000)

$('.alert').delay(timeout).fadeOut(300);
</script>
    