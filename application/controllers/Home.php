<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  public  function __construct()
  {
    parent::__construct();
    $this->load->library('session');
  $this->load->library('user_agent');
   $this->load->database();
    $this->load->model('User_model');
  $this->load->library('form_validation');
 $this->load->helper('form');
   $this->load->helper('url');
     
  }
   
public function index()
  {
    $data['categories'] = $this->User_model->get_categories();
   // echo "<pre>";
    // print_r($data['categories'][2]->subs[0]); exit;
    $data['product_feature']=$this->db->get_where('product',array('status'=>1))->result_array();
   
    $data['testimonial']=$this->db->get('testimonial')->result_array();
 
      $data['product']=$this->db->get('product')->result_array();
    $data['category1']=$this->db->get('category')->result_array();
    
    
     //$data['category_feature']=$this->db->get('primium_category')->result_array();

     $data['category_feature']=$this->db->get_where('category',array('status'=>1))->result_array();
    $data['slider']=$this->db->get('slider')->result_array();
  
    $this->load->view('index',$data);
   }

public function product($slug)
  {
  $product_details=$this->db->get_where('product',array('slug'=>$slug))->row_array();
      $id=$product_details['id'];
   //  echo  $id;exit;
      $category_id=$_GET['category_id'];
    $data['categories'] = $this->User_model->get_categories();
    $data['product_details']=$this->db->get_where('product',array('id'=>$id))->row_array();
   /* echo "<pre>";
    print_r($data['product_details']);exit;*/
     $data['product_details1']=$this->db->get_where('product',array('category_id'=>$data['product_details']['category_id']))->result_array();
       $data['product_image']=$this->db->get_where('product_image',array('product_id'=>$id))->result_array();
   // print_r($data['product_details']);exit;
  $this->load->view('product_details',$data);
   
  }

  public function contact_us()
  {
    $data['categories'] = $this->User_model->get_categories();
  $this->load->view('contact_us',$data);
   
  }
  
  public function send_mail() { 
         $from_email = $_POST['email']; 
         $to_email = "manishapatle09@gmail.com"; 
         
         $data = array(
                     
                   'name' => $_POST['name'],
                    'phone' => $_POST['phone'],
                     'subject' => $_POST['subject'],
                    'email' => $_POST['email'],
                 'message' => $_POST['message'],
                                      ); 
       
      $this->User_model->insertdata('contact',$data);
   
         //Load email library 
         $this->load->library('email'); 
   
         $this->email->from($from_email, $_POST['name']); 
         $this->email->to($to_email);
         $this->email->subject('Hirajevrat'); 
         $this->email->message($_POST['message']); 
   
         //Send mail 
         if($this->email->send()) 
         $this->session->set_flashdata('msg', 'Message Sent Successfully!...');
         
         else 
         $this->session->set_flashdata("msg","Error in sending Email."); 
        
          //$this->load->view('contact');   
          redirect(base_url('contact-us') ); 
      } 
  
  
  
  public function about_us()
  {
     //echo "hiii";exit;
    $data['categories'] = $this->User_model->get_categories();
  $this->load->view('about_us',$data);
   
  }
  
    public function shop($slug)
  { 
      $category_details=$this->db->get_where('category',array('slug'=>$slug))->row_array();
       $category_id=$category_details['id'];
      // echo $category_id;exit;
    $data['categories'] = $this->User_model->get_categories();
      $data['product_details']=$this->db->get_where('product',array('category_id'=>$category_id))->result_array();
     // echo "<pre>";
     // print_r($data['product_details']);exit;
  $this->load->view('shop',$data);
   
  }
  
public function insert_product()
{
 $product_details=$this->db->get_where('product',array('slug'=>$slug))->row_array();
      $id=$product_details['id'];
  $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
    $this->form_validation->set_rules('mobile_no', 'Mobile No', 'required');
     $this->form_validation->set_rules('estimate_quantity', 'Estimate Quantity', 'trim|required');
    $this->form_validation->set_rules('unit_type', 'Unit Type', 'required');
     $this->form_validation->set_rules('order_value', 'Order Value', 'trim|required');
    $this->form_validation->set_rules('reselling', 'Reselling', 'required');
      $this->form_validation->set_rules('requirement_details', 'Requirement Details', 'required');
     $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_message('required', 'Enter %s');

 if ($this->form_validation->run() === FALSE)
        {  
        
   $this->load->view('product_details',$data);
   
       }
        else
      { 
 // $time = time();

  $data = array(
                     
                'product_id' =>$_POST['product_id'],
                 'full_name' => $_POST['full_name'],
                  'mobile_no' => $_POST['mobile_no'],
                   'currency' => $_POST['currency'],
                  
                  'estimate_quantity' => $_POST['estimate_quantity'],
                  'unit_type' => $_POST['unit_type'],
                  'order_value' => $_POST['order_value'],
                  'reselling' => $_POST['reselling'],
                   'requirement_details' => $_POST['requirement_details'],
                
                   'enuiry_date'   =>  date('Y-m-d')

                    ); 
                 //   print_r($data);exit;
        
      $this->User_model->insertdata('product_enquiry',$data);
       $msg = "Enquiry Send Successfully!...";
  
     $this->session->set_flashdata('msg', "$msg");
       redirect($_POST['redirect_link']); 
       
     
      /*  echo json_encode(array(
        "statusCode"=>200
        ));*/
}

}

public function insert_enquiry()
{

  $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
    $this->form_validation->set_rules('mobile_no', 'Mobile No', 'required');
     $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required');
   
     $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_message('required', 'Enter %s');

 if ($this->form_validation->run() === FALSE)
        {  
        
   $this->load->view('index',$data);
   
       }
        else
      { 
 // $time = time();

  $data = array(
                     
                'product_id' =>$_POST['product_id'],
                 'full_name' => $_POST['full_name'],
                  'mobile_no' => $_POST['mobile_no'],
                  
                  'quantity' => $_POST['quantity'],
                
                   'enuiry_date'   =>  date('Y-m-d')

                    ); 
                 //   print_r($data);exit;
        
      $this->User_model->insertdata('enquiry',$data);
       $msg = "Enquiry Send Successfully!...";
  
     $this->session->set_flashdata('msg', "$msg");
       redirect(base_url('')); 
      /*  echo json_encode(array(
        "statusCode"=>200
        ));*/
}

}

 

}