<?php
      $basepath = base_url()."assets";
      $this->load->view('includes/header');
      $this->load->view('includes/sidebar');

?>
<!--End topbar header-->

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Manage Testimonial</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Admin</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Testimonial</a></li>
            <li class="breadcrumb-item active" aria-current="page">Manage Testimonial</li>
         </ol>
	   </div>
	   
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i>Manage Testimonial</div>
            <div class="card-body">
              <div class="table-responsive">
                <?php  
               if($msg=$this->session->flashdata('msg'))
               {
              // $msg_class=$this->session->flashdata('msg_class')
                ?> 
             <!--   <div class="alert <?php $msg_class;?>">  -->
                    <div class="alert alert-success"> 
                  <strong><?php echo $msg; ?></strong>
                  
                </div>
              <?php  } ?>
              <table id="default-datatable" class="table table-bordered">
                <thead>
                    <tr>
                      <th>Sr No.</th>
                      <th>Name</th>
                      <th>Image</th>
                      <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php  
                   $sr=0; 
                   foreach($records as $rec){ 
                  //  print_r($rec);exit;
                    $sr++; ?>
                   <tr>
                  <td><?php echo $sr;?></td>
                  <td><?php echo $rec->title;?></td>
                  <td><img src="../<?php echo $rec->image;?>" width="100px" height="50px"></td>
                  
                  <td>
                    
                    <a href="<?php echo base_url('Admin/edit_testimonial?id='.$rec->id);?>" onclick="return confirm('Do You Really Wants to Edit')"> <button type="button" class="btn btn-info waves-effect waves-light"><i class="fa fa-pencil"></i></button></a> 
                      <!-- <i style="font-size:20px; color: blue;margin-left: 10px;" class="fa fa-pencil" ></i> -->
                                          
                      <a href="<?php echo base_url('Admin/delete_testimonial?id='.$rec->id);?>" onclick="return confirm('Do You Really Wants to Delete')"><button type="button" class="btn btn-danger waves-effect waves-light"> <i class="fa fa fa-trash-o"></i>
                      <!-- <i style="font-size:20px; color: red;margin-left: 10px;" class="fa fa-trash"></i> --></button></a></td>
                </tr>
                <?php } ?>
                </tbody>
               
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->


    
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->
	<?php 
   $this->load->view('includes/footer');
?>
 <script type="text/javascript">
  var timeout = 3000; // in miliseconds (3*1000)

$('.alert').delay(timeout).fadeOut(300);
</script>
