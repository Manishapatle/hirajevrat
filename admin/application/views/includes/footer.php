<?php
      $basepath = base_url()."assets";
?>
   <footer class="footer">
      <div class="container">
        <div class="text-center">
          Copyright © 2020 Admin
        </div>
      </div>
    </footer>
  <!--End footer-->
   
  </div><!--End wrapper-->


  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo $basepath;?>/js/jquery.min.js"></script>
  <script src="<?php echo $basepath;?>/js/popper.min.js"></script>
  <script src="<?php echo $basepath;?>/js/bootstrap.min.js"></script>
  
  <!-- simplebar js -->
   <script src="<?php echo $basepath;?>/plugins/simplebar/js/simplebar.js"></script>
  <!-- waves effect js -->
  <script src="<?php echo $basepath;?>/js/waves.js"></script>
  <!-- sidebar-menu js -->
  <script src="<?php echo $basepath;?>/js/sidebar-menu.js"></script>
  <!-- Custom scripts -->
  <script src="<?php echo $basepath;?>/js/app-script.js"></script>
  <script src="<?php echo $basepath;?>/plugins/bootstrap-datatable/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo $basepath;?>/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo $basepath;?>/plugins/bootstrap-datatable/js/dataTables.buttons.min.js"></script>
</body>

<!-- Mirrored from codervent.com/rukada/color-admin/form-layouts.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 11 Aug 2020 06:38:34 GMT -->
</html>
  <script>
     $(document).ready(function() {
      //Default data table
       $('#default-datatable').DataTable();


       var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );
 
     table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
      
      } );

    </script>