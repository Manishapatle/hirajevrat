<?php
      $basepath = base_url()."assets";
?>

<body>

<!-- Start wrapper-->
 <div id="wrapper">
 
 <!--Start sidebar-wrapper-->
   <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
     <div class="brand-logo">
      <a href="<?php echo base_url('Admin/dashboard');?>">
       <!--<img src="<?php echo $basepath;?>/images/prakash-logo.png" class="logo-icon" alt="logo icon">-->
       <h5 class="logo-text"> hirajevrat</h5>
     </a>
   </div>
   <ul class="sidebar-menu do-nicescrol">
     <li class="sidebar-header">MAIN NAVIGATION</li>
      <li>
        <a href="<?php echo base_url('Admin/dashboard');?>" class="waves-effect">
          <i class="zmdi zmdi-view-dashboard"></i> <span>Dashboard</span>
        </a>
         </li>
     
      <li>
      
     <li>
        <a href="javaScript:void();" class="waves-effect">
          <i class="zmdi zmdi-map"></i> <span>Slider</span>
          <i class="fa fa-angle-left float-right"></i>
        </a>
        <ul class="sidebar-submenu">
          <li><a href="<?php echo base_url('Admin/add_slider');?>"><i class="zmdi zmdi-star-outline"></i> Add Slider</a></li>
          <li><a href="<?php echo base_url('Admin/view_slider');?>"><i class="zmdi zmdi-star-outline"></i> View Slider</a></li>
        </ul>
       </li>
       
        <li>
        <a href="javaScript:void();" class="waves-effect">
          <i class="zmdi zmdi-layers"></i> <span>Category</span>
          <i class="fa fa-angle-left float-right"></i>
        </a>
        <ul class="sidebar-submenu">
       <!--   <li><a href="<?php //echo base_url('Admin/add_category');?>"><i class="zmdi zmdi-star-outline"></i>Primium Category</a></li>
          <li><a href="<?php //echo base_url('Admin/view_category');?>"><i class="zmdi zmdi-star-outline"></i>Manage Category</a></li>-->
           <li><a href="<?php echo base_url('Admin/add_product_category');?>"><i class="zmdi zmdi-star-outline"></i>Product Category</a></li>
          <li><a href="<?php echo base_url('Admin/view_product_category');?>"><i class="zmdi zmdi-star-outline"></i>Manage Product Category </a></li>
        </ul>
       </li>
       
       
       <li>
        <a href="javaScript:void();" class="waves-effect">
          <i class="zmdi zmdi-layers"></i> <span>Product</span>
          <i class="fa fa-angle-left float-right"></i>
        </a>
        <ul class="sidebar-submenu">
        
           <li><a href="<?php echo base_url('Admin/add_product');?>"><i class="zmdi zmdi-star-outline"></i> Add Product</a></li>
          <li><a href="<?php echo base_url('Admin/view_product');?>"><i class="zmdi zmdi-star-outline"></i> Manage Product</a></li>
        </ul>
       </li>
       <li>
        <a href="javaScript:void();" class="waves-effect">
          <i class="zmdi zmdi-widgets"></i> <span>Testimonial</span>
          <i class="fa fa-angle-left float-right"></i>
        </a>
        <ul class="sidebar-submenu">
          <li><a href="<?php echo base_url('Admin/add_testimonial');?>"><i class="zmdi zmdi-star-outline"></i> Add Testimonial</a></li>
          <li><a href="<?php echo base_url('Admin/view_testimonial');?>"><i class="zmdi zmdi-star-outline"></i> View Testimonial</a></li>
        </ul>
       </li>
     <li>
        <a href="javaScript:void();" class="waves-effect">
          <i class="zmdi zmdi-collection-folder-image"></i> <span>Product Enquiry</span>
          <i class="fa fa-angle-left float-right"></i>
        </a>
        <ul class="sidebar-submenu">
        <!-- <li><a href="<?php //echo base_url('Admin/product_enquiry');?>"><i class="zmdi zmdi-star-outline"></i>Product Enquiry</a></li>-->
          <li><a href="<?php echo base_url('Admin/view_product_enquiry');?>"><i class="zmdi zmdi-star-outline"></i>Product Enquiry</a></li>
        </ul>
       </li> 
     <li>
        <a href="<?php echo base_url('Admin/login');?>" class="waves-effect">
          <i class="icon-power mr-2"></i> <span>Logout</span>
        </a>
      </li>

     
    </ul>
   
   </div>
   <!--End sidebar-wrapper-->

<!--Start topbar header-->
<header class="topbar-nav">
 <nav class="navbar navbar-expand fixed-top bg-white">
 
     
 
</nav>
</header>