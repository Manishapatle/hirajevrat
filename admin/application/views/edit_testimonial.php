<?php
      $basepath = base_url()."assets";
      $this->load->view('includes/header');
      $this->load->view('includes/sidebar');

?>
<!--End topbar header-->
<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
    <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Edit Testimonial</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Admin</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Testimonial</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit Testimonial</li>
         </ol>
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->


    <div class="row">
      <div class="col-lg-12">
         <div class="card">
           <div class="card-body">
           <div class="card-title text-primary">Edit Testimonial</div>
           <hr>
            <?php  
               if($msg=$this->session->flashdata('msg'))
               {
              // $msg_class=$this->session->flashdata('msg_class')
                ?> 
             <!--   <div class="alert <?php $msg_class;?>">  -->
                    <div class="alert alert-success"> 
                  <strong><?php echo $msg; ?></strong>
                  
                </div>
              <?php  } ?>
           <form method="post" action="<?php echo base_url().'Admin/update_testimonial?id='.$records->id; ?>" enctype= multipart/form-data>
           <div class="form-group">
            <label for="input-1">Name</label>
            <input type="text" name="title" class="form-control" placeholder="Enter Name" value="<?php echo $records->title;?>">
           </div>
           <div class="form-group">
              <label for="input-1">Description</label>
              <textarea type="text" id="myeditor" name="description" class="form-control"  placeholder="">
                <?php echo $records->description;?>
              </textarea>
              <span style="color: red;"><?php echo form_error('description'); ?></span>            
            </div>
           <div class="form-group">
            <label for="input-2">Image</label>
            <input class="form-control" type="file" name="file" value="<?php echo $records->image;?>" >
            
           </div>
         
           
           <div class="form-group">
            <button type="submit" class="btn btn-primary shadow-primary px-5"><i class="icon-lock"></i> Update</button>
          </div>
          </form>
         </div>
         </div>
      </div>

      
    </div><!--End Row-->


   

		
		  </div><!--End Row-->

   
    <!-- End container-fluid-->
    
   </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->
	<?php 
   $this->load->view('includes/footer');
?>
<script src="//cdn.ckeditor.com/4.5.9/full/ckeditor.js"></script>
  <script>
        //  For Ckeditor
    CKEDITOR.replace('myeditor');
  </script>
 <script type="text/javascript">
  var timeout = 3000; // in miliseconds (3*1000)

$('.alert').delay(timeout).fadeOut(300);
</script>