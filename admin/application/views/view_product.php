<?php
      $basepath = base_url()."assets";
      $this->load->view('includes/header');
      $this->load->view('includes/sidebar');

?>
<!--End topbar header-->

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Manage Product</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Admin</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Product</a></li>
            <li class="breadcrumb-item active" aria-current="page">Manage Product</li>
         </ol>
	   </div>
	   
     </div>
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <span style="position: absolute;top: 20px;"><i class="fa fa-table"></i>Manage Product</span>
              <a href="<?php echo base_url('Admin/add_product');?>"> <button type="submit" class="btn btn-primary px-5" style="float: right;">Add Product</button></a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <?php  
               if($msg=$this->session->flashdata('msg'))
               {
              // $msg_class=$this->session->flashdata('msg_class')
                ?> 
             <!--   <div class="alert <?php $msg_class;?>">  -->
                    <div class="alert alert-success"> 
                  <strong><?php echo $msg; ?></strong>
                  
                </div>
              <?php  } ?>
               <div class="row">
                   
                     <a href="<?php echo base_url().'Admin/export_csvproduct';?>" class="btn btn-primary">Export To Excel</a>
                 
                </div><br>
                <div class="row">
                   
                     <a href="<?php echo base_url().'Admin/product_zip';?>" class="btn btn-primary">Export To Zip Image</a>
                 
                </div><br>
                
              <table id="default-datatable" class="table table-bordered">
                <thead>
                    <tr>
                      <th>Sr No.</th>
                      <th>Category Name</th>
                      <th>Product Name</th>
                      <th>Price</th>
                     <!--  <th>Color</th>-->
                      <th>Image</th>
                      <th>Feature</th>
                      <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <?php  
                   $sr=0; 
                   foreach($recordss as $rec){ 
                  //  print_r($rec);exit;
                    $sr++; ?>
                   <tr>
                  <td><?php echo $sr;?></td>
                    <td><?php echo $rec->category;?></td>
                 <td><?php echo $rec->product_name;?></td>
                 <td><?php echo $rec->price;?></td>
               <!--  <td><?php// echo $rec->color;?></td>-->
                  <td><img src="<?php echo $rec->image;?>" width="50px" height="50px"></td>
                  <td>
                      <span id='active_<?=$rec->id?>'>
                              <?php if($rec->status==0){
                                  $st_s= "Hide";
                                  $css_class = "btn btn-danger active btn-block ";
                                }else{
                                  $st_s= "Show";
                                  $css_class = "btn btn-info active btn-block ";
                                }?>
                              <a class="btn btn-sm bg-<?=$css_class?>-light" href="javascript:;" onclick="get_approve('<?=$rec->id?>','<?=$rec->status?>','active_<?=$rec->id?>')" class="check" checked style="float: left;margin: 0 11px 0 1px; width: 80%;">
                                <?=$st_s?>
                              </a>
                      </span>

                  </td>
                  <td>
                     
                    <a href="<?php echo base_url('Admin/edit_product?id='.$rec->id);?>" onclick="return confirm('Do You Really Wants to Edit')"> <button type="button" class="btn btn-info waves-effect waves-light"><i class="fa fa-pencil"></i></button></a> 
                      <!-- <i style="font-size:20px; color: blue;margin-left: 10px;" class="fa fa-pencil" ></i> -->
                                          
                      <a href="<?php echo base_url('Admin/delete_product?id='.$rec->id);?>" onclick="return confirm('Do You Really Wants to Delete')"><button type="button" class="btn btn-danger waves-effect waves-light"> <i class="fa fa fa-trash-o"></i>
                      <!-- <i style="font-size:20px; color: red;margin-left: 10px;" class="fa fa-trash"></i> --></button></a></td>
                </tr>
                <?php } ?>
                </tbody>
               
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->


    
    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->
	<?php 
   $this->load->view('includes/footer');
?>
 <script type="text/javascript">
  var timeout = 3000; // in miliseconds (3*1000)

$('.alert').delay(timeout).fadeOut(300);
</script>
<script>

function get_approve(id,status,ele_id){
  if (status==1) {
    new_status=0;
    class_css = 'danger';
    st_str = "Hide";
  }else{
    new_status=1;
    class_css = 'success';
    st_str = "Show";
  }
  $.ajax({
    url: "<?php echo base_url("Admin/category_approve_unapprove");?>",
    type: "POST",
    cache: false,
    data:{'id':id,'status':status},
    success: function(data1){
      alert(data1);
      var new_str = `<a class="btn btn-sm bg-`+class_css+`-light" href="javascript:;" onclick="get_approve('`+id+`','`+new_status+`','`+ele_id+`')" class="check" checked style="float: left;margin: 0 11px 0 1px;">
                                `+st_str+`
                              </a>`;
      $('#'+ele_id).html(new_str);
    },
    error: function(error){
      console.log(error);
    }
  });
}

function get_feature(id1,status1,ele_id1){
  if (status1==1) {
    new_status1=0;
    class_css1 = 'danger';
    st_str1 = "Hide";
  }else{
    new_status1=1;
    class_css1 = 'success';
    st_str1 = "Show";
  }
  $.ajax({
    url: "<?php echo base_url("Admin/category_approve_feature");?>",
    type: "POST",
    cache: false,
    data:{'id':id1,'status':status1},
    success: function(data11){
      alert(data11);
      var new_str1 = `<a class="btn btn-sm bg-`+class_css1+`-light" href="javascript:;" onclick="get_feature('`+id1+`','`+new_status1+`','`+ele_id1+`')" class="check" checked style="float: left;margin: 0 11px 0 1px;">
        `+st_str1+`
        </a>`;
      $('#'+ele_id1).html(new_str1);
    },
    error: function(error){
      console.log(error);
    }
  });
}

</script>