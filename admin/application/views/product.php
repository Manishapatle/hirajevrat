<?php
      $basepath = base_url()."assets";
      $this->load->view('includes/header');
      $this->load->view('includes/sidebar');

?>
<!--End topbar header-->
<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
    <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Product</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Admin</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Product</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Product</li>
         </ol>
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->


    <div class="row">
      <div class="col-lg-12">
         <div class="card">
           <div class="card-body">
             <div class="card-title text-primary">Product</div>
             <hr>
            <?php  
               if($msg=$this->session->flashdata('msg'))
               {
              // $msg_class=$this->session->flashdata('msg_class')
                ?> 
             <!--   <div class="alert <?php $msg_class;?>">  -->
                    <div class="alert alert-success"> 
                      <strong><?php echo $msg; ?></strong>
                    </div>
              <?php  } ?>
              <form method="post" action="<?php echo base_url().'Admin/insert_product'; ?>" enctype= multipart/form-data>
                 <div class="form-group">
                    <label for="input-1">Category</label>
                     <select class="form-control " name="category_id" required>
						<option selected="">---Select Category----</option>
						  <?php
						    foreach($category as $row)
						      {
		                         echo '<option value="'.$row->id.'">'.$row->category.'</option>';
     	                        }
						 ?>
					 </select>
                    </select>
                 </div>
                 <div class="form-group col-md-6 space-product" style="float: left;">
                    <label for="input-1">Product Name</label>
                    <input type="text" name="product_name" class="form-control" id="three" placeholder="Enter Product Name" >
                    <input type="hidden" id="four" name="slug">
                 </div>
                 <div class="form-group col-md-6 space-product" style="float: left;">
                    <label for="input-1">Price</label>
                    <input type="text" name="price" class="form-control" id="input-1" placeholder="Enter Price" >
                 </div>
                 <div class="form-group space-product">
                    <label for="input-2">Product Image</label>
                    <input type="file" name="file" class="form-control" id="input-2">
                 </div>
                    <div class="form-group space-product">
                    <label for="input-1">Short Description</label>
                    <textarea type="text" id="myeditor1" name="short_description" class="form-control"  placeholder=""></textarea>
                    <span style="color: red;"><?php echo form_error('short_description'); ?></span>            
                 </div>
                 
                 <div class="form-group space-product">
                    <label for="input-1">Description</label>
                    <textarea type="text" id="myeditor" name="description" class="form-control"  placeholder=""></textarea>
                    <span style="color: red;"><?php echo form_error('description'); ?></span>            
                 </div>
                 
               
                	
						<div class="form-group col-md-6 space-product" style="float: left;">
        										<div class="form-group" id="dealer">
        											<label class="form-label text-dark">Product Slider Images</label><br>
        											<input type="file" name="file1[]" multiple="multiple" style="margin: 0 0 12px 0;" required>
        										</div>
        											<button class="btn btn-success" type="button" onclick="add_new()">Add More Image+</button>
        									</div>
        									
        									
        							
        								
        									<script type="text/javascript">
                                                function add_new(){
                                                  var row_str = `	<input type="file" name="file1[]" style="margin: 0 0 12px 0px; ">`;
                
                                                $('#dealer').append(row_str);
                
                                                }
                                            </script>
                                            <div style="clear:both"></div>
                 <div class="col-md-12 space-product">
                     <b><u>Additional Information</u></b><br><br>
                
                 <div class="form-group col-md-3" style="float: left;">
                    <label for="input-1">Style no.</label>
                    <input type="text" name="style_no" class="form-control" id="input-1" placeholder="Enter Style No." >
                 </div>
                  <div class="form-group col-md-3" style="float: left;">
                    <label for="input-1">Metal</label>
                    <input type="text" name="metal" class="form-control" id="input-1" placeholder="Enter Metal" >
                 </div>
                  <div class="form-group col-md-3" style="float: left;">
                    <label for="input-1">Stamped</label>
                    <input type="text" name="stamped" class="form-control" id="input-1" placeholder="Enter Stamped" >
                 </div>
                  <div class="form-group col-md-3" style="float: left;">
                    <label for="input-1">Conclusion</label>
                    <input type="text" name="conclusion" class="form-control" id="input-1" placeholder="Enter Conclusion" >
                 </div>
                 <div class="form-group col-md-3" style="float: left;">
                    <label for="input-1">Shape</label>
                    <input type="text" name="shape" class="form-control" id="input-1" placeholder="Enter Shape" >
                 </div>
                 <div class="form-group col-md-3" style="float: left;">
                    <label for="input-1">Color</label>
                    <input type="text" name="color" class="form-control" id="input-1" placeholder="Enter Color" >
                 </div>
                 <div class="form-group col-md-3" style="float: left;">
                    <label for="input-1">Clarity</label>
                    <input type="text" name="clarity" class="form-control" id="input-1" placeholder="Enter Clarity" >
                 </div>
                 <div class="form-group col-md-3" style="float: left;">
                    <label for="input-1">Est. weight</label>
                    <input type="text" name="weight" class="form-control" id="input-1" placeholder="Enter Weight" >
                 </div>
                 
                 <div class="form-group col-md-3" style="float: left;">
                    <label for="input-1">Total Est. weight</label>
                    <input type="text" name="total_weight" class="form-control" id="input-1" placeholder="Enter Total Est. Weight" >
                 </div>
                 
                 <div class="form-group col-md-3" style="float: left;">
                    <label for="input-1">Certificate No</label>
                    <input type="text" name="certificate_no" class="form-control" id="input-1" placeholder="Enter Certificate No" >
                 </div>
                 
                  <div class="form-group col-md-6" style="float: left;">
                    <label for="input-1">Comment</label>
                    <input type="text" name="comment" class="form-control" id="input-1" placeholder="Enter Comment" >
                 </div>
                 </div>
                 
                   
                 <div class="form-group">
                    <button type="submit" class="btn btn-primary shadow-primary px-5"><i class="icon-lock"></i> Submit</button>
                </div>
              </form>
         </div>
         </div>
      </div>

      
    </div><!--End Row-->
  </div><!--End Row-->
</div>
    <!-- End container-fluid-->
    
   
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->
	<?php 
   $this->load->view('includes/footer');
?>
 <script src="//cdn.ckeditor.com/4.5.9/full/ckeditor.js"></script>
  <script>
        //  For Ckeditor
    CKEDITOR.replace('myeditor');
  </script>
  <script>
        //  For Ckeditor
    CKEDITOR.replace('myeditor1');
  </script>
  <script>
  $(function () {
    var $src = $('#three'),
        $dst = $('#four');
    $src.on('input', function () {
        $dst.val($src.val());
    });
});
  </script>
 <script type="text/javascript">
  var timeout = 3000; // in miliseconds (3*1000)

$('.alert').delay(timeout).fadeOut(300);
</script>
 