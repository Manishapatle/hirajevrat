<?php
      $basepath = base_url()."assets";
      $this->load->view('includes/header');
      $this->load->view('includes/sidebar');

?>
<!--End topbar header-->
<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
    <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Product Category</h4>
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javaScript:void();">Admin</a></li>
            <li class="breadcrumb-item"><a href="javaScript:void();">Product Category</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Product Category</li>
         </ol>
	   </div>
	  
     </div>
    <!-- End Breadcrumb-->


    <div class="row">
      <div class="col-lg-12">
         <div class="card">
           <div class="card-body">
           <div class="card-title text-primary">Product Category</div>
           <hr>
            <?php  
               if($msg=$this->session->flashdata('msg'))
               {
              // $msg_class=$this->session->flashdata('msg_class')
                ?> 
             <!--   <div class="alert <?php $msg_class;?>">  -->
                    <div class="alert alert-success"> 
                  <strong><?php echo $msg; ?></strong>
                  
                </div>
              <?php  } ?>
           <form method="post" action="<?php echo base_url().'Admin/insert_product_category'; ?>" enctype= multipart/form-data>
           <div class="form-group">
            <label for="input-1">Category</label>
            <input type="text" name="category" class="form-control" id="three" placeholder="Enter Category" >
            <input type="hidden" id="four" name="slug">
          
           </div>
           
            <div class="form-group">
                    <label for="input-2">Image</label>
                    <input type="file" name="file" class="form-control" id="input-2">
                 </div>
                 
                  <div class="form-group">
                    <label for="input-2">Feature Image</label>
                    <input type="file" name="file1" class="form-control" id="input-2">
                 </div>
           
           <div class="form-group">
            <button type="submit" class="btn btn-primary shadow-primary px-5"><i class="icon-lock"></i> Submit</button>
          </div>
          </form>
        
         </div>
      </div>

      
    </div><!--End Row-->


   

		
		  </div><!--End Row-->

    </div>
    <!-- End container-fluid-->
    
   </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
	
	<!--Start footer-->
	<?php 
   $this->load->view('includes/footer');
?>
 <script type="text/javascript">
  var timeout = 3000; // in miliseconds (3*1000)

$('.alert').delay(timeout).fadeOut(300);
</script>

<script>
  $(function () {
    var $src = $('#three'),
        $dst = $('#four');
    $src.on('input', function () {
        $dst.val($src.val());
    });
});
  </script>