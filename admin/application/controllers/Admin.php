<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	 public  function __construct()
  {
    parent::__construct();
    $this->load->library('session');
   $this->load->database();
    $this->load->model('Admin_model');
  $this->load->library('form_validation');
   $this->load->helper('form');
   $this->load->helper('url');
  }
  
 public function index()
  {
    //echo "hiii";exit;
    //$this->load->view('include/header');
    $this->load->view('login');
    //$this->load->view('include/footer');
  }
  public function signin()
   {
    //echo "hiii";exit;
          $username  = $_POST['username'];

           $password = $_POST['password'];
                     //echo $password;exit;
       $userinfo = $this->Admin_model->validate_login($_POST['username'],$_POST['password']);
   //print_r($userinfo) ;exit;
      $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
          $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_message('required', 'Enter %s');
        if ($this->form_validation->run() === FALSE)
        {  
            $this->load->view('login');
        }
        else
        {
           if($userinfo>0)
    { 
      $data = $this->session->has_userdata('username');
  
          redirect(base_url('Admin/dashboard'));
      
    }
 else 
     {
   $data['error']="<h6 style='color:red'>Invalid Username or Password !</h6>";
          
      
     }
     $this->load->view('login',$data);
        }
      

    }


  public function logout()
   {

    unset($_SESSION['login']);
  $this->session->sess_destroy();
   redirect(base_url());
   }
  
	public function dashboard()
	{
    //echo "hiii";
		$this->load->view('dashboard');
	}
  public function login()
  {
    //echo "hiii";
    $this->load->view('login');
  }

  /**************Slider*******************/
  public function add_slider()
  {
    //echo "hiii";
    $this->load->view('slider');
  }
  
  
  
 private function set_upload_options()
{   
    
    $config = array(
     
    $_SERVER['DOCUMENT_ROOT'].'/hirajevrat/admin/uploads/',
      'allowed_types' => "gif|jpg|png|jpeg",
      'max_size' => '0',
      'overwrite' => FALSE
      
    );
    //upload an image options
   /* $config = array();
    $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/uploads/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size']      = '0';
    $config['overwrite']     = FALSE;*/

    return $config;
}
  
  
  
  public function insert_slider()
  {
  
 // print_r($_POST);exit;
   if(isset($_SESSION['login']) && $_SESSION['login'] == true)
    {
       $this->form_validation->set_rules('status', 'Status', 'trim|required');
  if ($this->form_validation->run() === FALSE)
        {  
        
    $this->load->view('add_slider');
   
        }
        else
        {   
          
        $this->load->library('upload');
   $config = array(
    // 'base_url' => 'http://'.$_SERVER['SERVER_NAME'].'/Admin/uploads/',
    'upload_path' => $_SERVER['DOCUMENT_ROOT'].'/hirajevrat/admin/uploads/',
    // 'upload_path' => './Admin/uploads/ ',
      'allowed_types' => "gif|jpg|png|jpeg",
      'file' => 'file'
    );
//print_r($config);exit;



   $this->load->library('upload', $config);
        $this->upload->initialize($config);
    if($this->upload->do_upload('file'))
    {
      $feild=$this->upload->data();
   //  $path=base_url("/uploads/".$feild['raw_name'].$feild['file_ext']);
    // echo $path;exit;
       $path="/uploads/".$feild['raw_name'].$feild['file_ext'];
   //echo $path;exit;
    }
    else
    {   
             $error = array('error' => $this->upload->display_errors());
           // print_r($error);exit;
             $msg = "Please Select Media image in gif|jpg|png|jpeg Format ";
             $this->session->set_flashdata('msg', "$msg");

               redirect(base_url('Admin/add_slider') ); 
    }
               $data = array(
                    'status' => '1',
                   
                    'image' =>$path
                 
               );
          
      $this->Admin_model->insertdata('slider',$data);
      $this->session->set_flashdata('msg', 'Slider Added Successfully!...');

               redirect(base_url('Admin/add_slider') ); 
           }

}
else{
     
       redirect(base_url());
    }

}
  
 
  public function view_slider()
  {
    $data['records'] = $this->Admin_model->selectdata('slider');
    // print_r($data);exit;
    $this->load->view('view_slider',$data);
  }
  public function delete_slider()
  {
  
      $id=$_GET['id'];
    $this->Admin_model->deletedata('slider',$id);
     $this->session->set_flashdata('msg', 'Your Record has been Deleted !...');
      redirect(base_url('Admin/view_slider') ); 
  
  }
   public function edit_slider()
   {
      $id=$_GET['id'];
      $data['records'] = $this->Admin_model->get_record('slider as ne','ne.id="'.$id.'"');
      $this->load->view('edit_slider',$data);
   }
  public function update_slider()
    {

    if(isset($_SESSION['login']) && $_SESSION['login'] == true)
    {

 $id=$_GET['id'];

    $this->load->library('upload');
   $config = array(
   
    'upload_path' => $_SERVER['DOCUMENT_ROOT'].'/hirajevrat/admin/uploads',
  
      'allowed_types' => "gif|jpg|png|jpeg",
      'file' => 'file'
    );
   $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
    if ($_FILES['file']['name']) {
    if($this->upload->do_upload('file'))
    {
      $feild=$this->upload->data();
    //$path=base_url("/uploads/".$feild['raw_name'].$feild['file_ext']);
     
       $path="/uploads/".$feild['raw_name'].$feild['file_ext'];
   //echo $path;exit;
        $data['image'] = $path;
    }
    else
    {   
             $error = array('error' => $this->upload->display_errors());
           // print_r($error);exit;
             $msg = "Please Select Media image in gif|jpg|png|jpeg Format ";
             $this->session->set_flashdata('msg', "$msg");

               redirect(base_url('Admin/view_slider') ); 
    }
    }

        $data['status'] = '1';
      
  
      $this->Admin_model->updatedata('slider',$data,$id);
      //print_r($this->db->last_query());exit;
       $this->session->set_flashdata('msg', 'Data Updated Successfully !...');
    
  redirect(base_url('Admin/view_slider') ); 
   }
  else{
      redirect(base_url());
    }
}


 

/****************Product Category***********************/



public function add_product_category()
  {
    //echo "hiii";
    $this->load->view('product_category');
  }
  public function insert_product_category()
 {
    if(isset($_SESSION['login']) && $_SESSION['login'] == true)
    {
      
       $this->load->library('upload');
   $config = array(
    // 'base_url' => 'http://'.$_SERVER['SERVER_NAME'].'/Admin/uploads/',
    'upload_path' => $_SERVER['DOCUMENT_ROOT'].'/hirajevrat/uploads/',
    // 'upload_path' => './Admin/uploads/ ',
      'allowed_types' => "gif|jpg|png|jpeg",
      'file' => 'file',
     
    );
//print_r($config);exit;
   $this->load->library('upload', $config);
        $this->upload->initialize($config);
     $file_path11 = "";
            if ($_FILES['file']['name']) {
                $output_dir ="uploads/";
                $filenamekey = time().'_'.rand(100,999);
                $filenamekey .= "." . pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);

                move_uploaded_file($_FILES["file"]["tmp_name"],$output_dir.$filenamekey);
                 $file_path11 =  base_url('uploads/'.$filenamekey);
                // $this->session->set_flashdata('msg', 'Image Uploaded Successfully!!...');
                // session()->setFlashdata('message', 'Uploaded Successfully!');
              }else{
                 // $file_path =  base_url('uploads/doctor-home-default.png');
              }
              
               $file_path22 = "";
            if ($_FILES['file1']['name']) {
                $output_dir ="uploads/";
                $filenamekey = time().'_'.rand(100,999);
                $filenamekey .= "." . pathinfo($_FILES["file1"]["name"], PATHINFO_EXTENSION);

                move_uploaded_file($_FILES["file1"]["tmp_name"],$output_dir.$filenamekey);
                 $file_path22 =  base_url('uploads/'.$filenamekey);
                // $this->session->set_flashdata('msg', 'Image Uploaded Successfully!!...');
                // session()->setFlashdata('message', 'Uploaded Successfully!');
              }else{
                 // $file_path =  base_url('uploads/doctor-home-default.png');
              }
    
               $data = array(
              'category' => $_POST['category'],
                'slug' => str_replace(" ", "-", $_POST['slug'])
               
              /*  'image' =>$path,
                  'feature_image' =>$path1*/
                
                 
               );
               
                $data['image'] = $file_path11;
                 $data['feature_image'] = $file_path22;
          
       
      $this->Admin_model->insertdata('category',$data);
      $this->session->set_flashdata('msg', 'Category Added Successfully!...');

              redirect(base_url('Admin/add_product_category') );  
        
        
        
           }
      
 else{
      redirect(base_url());
    }

    
  }  
public function view_product_category()
  {
  $data['records'] = $this->Admin_model->selectdata('category');
 // print_r($data);exit;
  $this->load->view('view_product_category',$data);
  }
public function delete_product_category()
  {
  
      $id=$_GET['id'];
    $this->Admin_model->deletedata('category',$id);
     $this->session->set_flashdata('msg', 'Your Record has been Deleted !...');
      redirect(base_url('Admin/view_product_category') ); 
  }
   public function edit_product_category()
   {
   
      $id=$_GET['id'];
      $data['records'] = $this->Admin_model->get_record('category as ne','ne.id="'.$id.'"');
      $this->load->view('edit_product_category',$data);
   }
    
   public function update_product_category()
   {
   
      $id=$_GET['id'];
      
        $this->load->library('upload');
   $config = array(
    // 'base_url' => 'http://'.$_SERVER['SERVER_NAME'].'/Admin/uploads/',
    'upload_path' => $_SERVER['DOCUMENT_ROOT'].'/hirajevrat/uploads/',
    // 'upload_path' => './Admin/uploads/ ',
      'allowed_types' => "gif|jpg|png|jpeg",
      'file' => 'file',
      
    );
    
  
//print_r($config);exit;
   $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
           $file_path11 = "";
            if ($_FILES['file']['name']) {
                $output_dir ="uploads/";
                $filenamekey = time().'_'.rand(100,999);
                $filenamekey .= "." . pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);

                move_uploaded_file($_FILES["file"]["tmp_name"],$output_dir.$filenamekey);
                $file_path11 =  base_url('uploads/'.$filenamekey);
              }
              
              
               $file_path22 = "";
            if ($_FILES['file1']['name']) {
                $output_dir ="uploads/";
                $filenamekey = time().'_'.rand(100,999);
                $filenamekey .= "." . pathinfo($_FILES["file1"]["name"], PATHINFO_EXTENSION);

                move_uploaded_file($_FILES["file1"]["tmp_name"],$output_dir.$filenamekey);
                $file_path22 =  base_url('uploads/'.$filenamekey);
              }
              
   
      $data['category'] = $_POST['category'];
      $data['slug'] = str_replace(" ", "-", $_POST['slug']);
      
       if ($file_path11) {
     $data['image'] =  $file_path11;
    }
    
      if ($file_path22) {
     $data['feature_image'] =  $file_path22;
    }
    
     $this->Admin_model->updatedata("category", $data,$id);
   
    $this->session->set_flashdata('msg', 'Data Updated Successfully !...');
    
  redirect(base_url('Admin/view_product_category') ); 
      
  
 
    }



/****************Product***********************/



public function add_product()
  {
    //echo "hiii";
    $data['category'] = $this->Admin_model->fetch_category();
    $this->load->view('product',$data);
  }
  public function insert_product()
  {
  
 // print_r($_POST);exit;
   if(isset($_SESSION['login']) && $_SESSION['login'] == true)
    {
       $this->form_validation->set_rules('description', 'Description', 'trim|required');
  if ($this->form_validation->run() === FALSE)
        {  
        
    $this->load->view('add_product');
   
        }
        else
        {   
          
        $this->load->library('upload');
   $config = array(
    // 'base_url' => 'http://'.$_SERVER['SERVER_NAME'].'/Admin/uploads/',
    'upload_path' => $_SERVER['DOCUMENT_ROOT'].'/hirajevrat/uploads/',
    // 'upload_path' => './Admin/uploads/ ',
      'allowed_types' => "gif|jpg|png|jpeg",
      'file' => 'file'
     
    );
//print_r($config);exit;
   $this->load->library('upload', $config);
        $this->upload->initialize($config);
    $path = "";
            if ($_FILES['file']['name']) {
                $output_dir ="uploads/";
                $filenamekey = time().'_'.rand(100,999);
                $filenamekey .= "." . pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);

                move_uploaded_file($_FILES["file"]["tmp_name"],$output_dir.$filenamekey);
                 $file_path =  base_url('uploads/'.$filenamekey);
                // $this->session->set_flashdata('msg', 'Image Uploaded Successfully!!...');
                // session()->setFlashdata('message', 'Uploaded Successfully!');
              }else{
                 // $file_path =  base_url('uploads/doctor-home-default.png');
              }
    
    
               $data = array(
                'category_id' => $_POST['category_id'],
                'product_name' => $_POST['product_name'],
                'price' => $_POST['price'],
                'description' => $_POST['description'],
                'short_description' => $_POST['short_description'],
                'style_no' => $_POST['style_no'],
                'metal' => $_POST['metal'],
                'stamped' => $_POST['stamped'],
                'conclusion' => $_POST['conclusion'],
                'shape' => $_POST['shape'],
                'color' => $_POST['color'],
                'clarity' => $_POST['clarity'],
                'weight' => $_POST['weight'],
                'total_weight' => $_POST['total_weight'],
                'comment' => $_POST['comment'],
                 'certificate_no' => $_POST['certificate_no'],
                'slug' => str_replace(" ", "-", $_POST['slug']),
               // 'image' =>$path
               /* 'image1' =>$path1,
                'image2' =>$path2,
                'image3' =>$path3,
                'image4' =>$path4*/
                 
               );
           $data['image'] =  $file_path;
      $this->Admin_model->insertdata('product',$data);
     $insert_id=$this->db->insert_id();
      $files = $_FILES;
        $count = count($_FILES['file1']['name']);
      
      for ($i=0; $i < $count; $i++) { 
          
         
            $file_path = "";
            if ($_FILES['file1']['name'][$i]) {
                $output_dir ="uploads/";
                $filenamekey = time().'_'.rand(100,999);
                $filenamekey .= "." . pathinfo($_FILES["file1"]["name"][$i], PATHINFO_EXTENSION);

                move_uploaded_file($_FILES["file1"]["tmp_name"][$i],$output_dir.$filenamekey);
                $file_path = base_url('uploads/'.$filenamekey);
              }
       
            $data4 = array();
            $data4['product_id'] = $insert_id;
           $data4['product_iamge'] = $file_path."";
        

          $this->Admin_model->insertdata("product_image", $data4);
      
          }
  $this->session->set_flashdata('msg', 'Product Added Successfully!...');

               redirect(base_url('Admin/add_product') ); 
           }

}
else{
     
       redirect(base_url());
    }

}
public function view_product()
  {
    $data['recordss']=$this->Admin_model->join2tableRecords('category as st','product as ci','st.id,st.category,ci.*','st.id=ci.category_id');
  //$data['recordss'] = $this->Admin_model->selectdata('product');
 // print_r($data);exit;
  $this->load->view('view_product',$data);
  }
public function delete_product()
  {
  
      $id=$_GET['id'];
    $this->Admin_model->deletedata('product',$id);
     $this->session->set_flashdata('msg', 'Your Record has been Deleted !...');
      redirect(base_url('Admin/view_product') ); 
  }
   public function edit_product()
   {
   
      $id=$_GET['id'];
        $data['category'] = $this->Admin_model->fetch_category();
    $data['records']=$this->Admin_model->join2tableRecord('category as st','product as ci','st.id,st.category,ci.*','st.id=ci.category_id','ci.id="'.$id.'"');
     $data['records11']=$this->db->get_where('product_image',array('product_id'=>$id))->result();
 // $data['records'] = $this->Admin_model->get_record('product as ne','ne.id="'.$id.'"');
   
     
     $this->load->view('edit_product',$data);
   }
   
     public function update_product()
   {
     
    
      $id=$_GET['id'];
       
        $this->load->library('upload');
   $config = array(
    // 'base_url' => 'http://'.$_SERVER['SERVER_NAME'].'/Admin/uploads/',
    'upload_path' => $_SERVER['DOCUMENT_ROOT'].'/hirajevrat/uploads/',
    // 'upload_path' => './Admin/uploads/ ',
      'allowed_types' => "gif|jpg|png|jpeg",
      'file' => 'file'
     
    );
//print_r($config);exit;
   $this->load->library('upload', $config);
        $this->upload->initialize($config);
    $path = "";
            if ($_FILES['file']['name']) {
                $output_dir ="uploads/";
                $filenamekey = time().'_'.rand(100,999);
                $filenamekey .= "." . pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);

                move_uploaded_file($_FILES["file"]["tmp_name"],$output_dir.$filenamekey);
                 $file_path =  base_url('uploads/'.$filenamekey);
              }
                 //echo $file_path."<br><pre>";print_r($_FILES);exit;
    
    
    
               $data = array(
                'category_id' => $_POST['category_id'],
                'product_name' => $_POST['product_name'],
                'price' => $_POST['price'],
                 'short_description' => $_POST['short_description'],
                'description' => $_POST['description'],
                'style_no' => $_POST['style_no'],
                'metal' => $_POST['metal'],
                'stamped' => $_POST['stamped'],
                'conclusion' => $_POST['conclusion'],
                'shape' => $_POST['shape'],
                'color' => $_POST['color'],
                'clarity' => $_POST['clarity'],
                'weight' => $_POST['weight'],
                 'total_weight' => $_POST['total_weight'],
                'comment' => $_POST['comment'],
                 'certificate_no' => $_POST['certificate_no'],
                'slug' => str_replace(" ", "-", $_POST['slug'])
              
                 
               );
              // print_r($data);exit;
           if ($file_path) {
     $data['image'] =  $file_path;
    }
    
      $this->Admin_model->updatedata1('product',$data,$id);
      $data['records']=$this->Admin_model->join2tableRecord('category as st','product as ci','st.id,st.category,ci.*','st.id=ci.category_id','ci.id="'.$id.'"');
     $insert_id = $data['records']->id;
  
        $records11=$this->db->get_where('product_image',array('product_id'=>$id))->result_array();
     
      $files = $_FILES;
        $count = count($_FILES['file1']['name']);
      
      for ($i=0; $i < $count; $i++) { 
          
         
            $file_path11 = "";
            if ($_FILES['file1']['name'][$i]) {
                $output_dir ="uploads/";
                $filenamekey = time().'_'.rand(100,999);
                $filenamekey .= "." . pathinfo($_FILES["file1"]["name"][$i], PATHINFO_EXTENSION);

                move_uploaded_file($_FILES["file1"]["tmp_name"][$i],$output_dir.$filenamekey);
                $file_path11 = base_url('uploads/'.$filenamekey);
              }
       
            $data4 = array();
            $data4['product_id'] = $insert_id;
             if ($file_path11) {
              
                 $data4['product_iamge'] =$file_path11;
             }
             
             
               if (isset($records11[$i])) {
            $this->Admin_model->updatedata("product_image", $data4,$records11[$i]['id']);
          }else{
            if ($file_path11) {
               $data4['product_iamge'] =$file_path11;
             }else{
           //$data4['product_iamge'] =$file_path11;
             }
             
            
          $this->Admin_model->insertdata("product_image", $data4);
          }
             
             
        

          
      
          }
    
    
      $this->session->set_flashdata('msg', 'Product Updated Successfully!...');

               redirect(base_url('Admin/view_product') ); 
      
     
 }

   
   
   
   public function category_approve_unapprove()
  {
 
        $id=$_POST['id'];
        if ($_POST['status']==1) {
          $status = 0;
          $message = "Hide Successfully";
        }else{
          $status = 1;
          $message = "Show Successfully";
        }
  
       $data = array(
       'status' => $status
      );
      
      $this->Admin_model->updatedata('product',$data,$id);
      echo $message;
}
  

 
   public function category_approve_unapprove1()
  {
 
        $id=$_POST['id'];
        if ($_POST['status']==1) {
          $status = 0;
          $message = "Hide Successfully";
        }else{
          $status = 1;
          $message = "Show Successfully";
        }
  
       $data = array(
       'status' => $status
      );
      
      $this->Admin_model->updatedata('category',$data,$id);
      echo $message;
}


/****************Testimonial***********************/



public function add_testimonial()
  {
    //echo "hiii";
    $this->load->view('testimonial');
  }
  public function insert_testimonial()
 {
  
   $this->form_validation->set_rules('description', 'description', 'trim|required');
      
  if ($this->form_validation->run() === FALSE)
        {  
        
    $this->load->view('add_testimonial');
   
        }
        else
        {   
          
        $this->load->library('upload');
   $config = array(
    
    'upload_path' => $_SERVER['DOCUMENT_ROOT'].'/hirajevrat/admin/uploads/testimonial',
  
      'allowed_types' => "gif|jpg|png|jpeg",
      'file' => 'file'
    );
   $this->load->library('upload', $config);
        $this->upload->initialize($config);
    if($this->upload->do_upload('file'))
    {
      $feild=$this->upload->data();
   //  $path=base_url("/uploads/".$feild['raw_name'].$feild['file_ext']);
    // echo $path;exit;
       $path="/uploads/testimonial/".$feild['raw_name'].$feild['file_ext'];
       
         $data['image'] = $path;
   
    }
    else
    {   
             $error = array('error' => $this->upload->display_errors());
           // print_r($error);exit;
             $msg = "Please Select Media image in gif|jpg|png|jpeg Format ";
             $this->session->set_flashdata('msg', "$msg");

               redirect(base_url('Login/add_testimonial') ); 
    }
              $data['title'] = $_POST['title'];
              $data['description'] = $_POST['description'];

              
          
      $this->Admin_model->insertdata('testimonial',$data);
   
      $this->session->set_flashdata('msg', 'Testimonial Added Successfully!...');

               redirect(base_url('Admin/add_testimonial') ); 
           }
           
  }
public function view_testimonial()
  {
  $data['records'] = $this->Admin_model->selectdata('testimonial');
 // print_r($data);exit;
  $this->load->view('view_testimonial',$data);
  }
public function delete_testimonial()
  {
  
      $id=$_GET['id'];
    $this->Admin_model->deletedata('testimonial',$id);
     $this->session->set_flashdata('msg', 'Your Record has been Deleted !...');
      redirect(base_url('Admin/view_testimonial') ); 
  }
   public function edit_testimonial()
   {
   
      $id=$_GET['id'];
      
    
  $data['records'] = $this->Admin_model->get_record('testimonial as ne','ne.id="'.$id.'"');
   
     
     $this->load->view('edit_testimonial',$data);
   }
    
  

   public function update_testimonial()
   {
   


   $id=$_GET['id'];

    $this->load->library('upload');
   $config = array(
   
    'upload_path' => $_SERVER['DOCUMENT_ROOT'].'/hirajevrat/admin/uploads/testimonial',
  
      'allowed_types' => "gif|jpg|png|jpeg",
      'file' => 'file'
    );
   $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
    if ($_FILES['file']['name']) {
    if($this->upload->do_upload('file'))
    {
      $feild=$this->upload->data();
    //$path=base_url("/uploads/".$feild['raw_name'].$feild['file_ext']);
     
       $path="/uploads/testimonial/".$feild['raw_name'].$feild['file_ext'];
   //echo $path;exit;
        $data['image'] = $path;
    }
    else
    {   
             $error = array('error' => $this->upload->display_errors());
           // print_r($error);exit;
             $msg = "Please Select Media image in gif|jpg|png|jpeg Format ";
             $this->session->set_flashdata('msg', "$msg");

               redirect(base_url('Admin/view_testimonial') ); 
    }
    }

        $data['title'] = $_POST['title'];
        $data['description'] = $_POST['description'];
              
      $this->Admin_model->updatedata('testimonial',$data,$id);
      //print_r($this->db->last_query());exit;
       $this->session->set_flashdata('msg', 'Data Updated Successfully !...');
    
  redirect(base_url('Admin/view_testimonial') ); 
  
}

/**************************Advertisement******************************/


public function add_advertisement()
  {
    //echo "hiii";
    $this->load->view('advertisement');
  }
  public function insert_advertisement()
 {
  
   $this->form_validation->set_rules('description', 'description', 'trim|required');
      
  if ($this->form_validation->run() === FALSE)
        {  
        
    $this->load->view('add_advertisement');
   
        }
        else
        {   
          
        $this->load->library('upload');
   $config = array(
    
    'upload_path' => $_SERVER['DOCUMENT_ROOT'].'/hirajevrat/admin/uploads/add',
  
      'allowed_types' => "gif|jpg|png|jpeg",
      'file' => 'file'
    );
   $this->load->library('upload', $config);
        $this->upload->initialize($config);
    if($this->upload->do_upload('file'))
    {
      $feild=$this->upload->data();
   //  $path=base_url("/uploads/".$feild['raw_name'].$feild['file_ext']);
    // echo $path;exit;
       $path="/uploads/add/".$feild['raw_name'].$feild['file_ext'];
       
         $data['image'] = $path;
   
    }
    else
    {   
             $error = array('error' => $this->upload->display_errors());
           // print_r($error);exit;
             $msg = "Please Select Media image in gif|jpg|png|jpeg Format ";
             $this->session->set_flashdata('msg', "$msg");

               redirect(base_url('Login/add_advertisement') ); 
    }
              $data['title'] = $_POST['title'];
              $data['description'] = $_POST['description'];

              
          
      $this->Admin_model->insertdata('advertisement',$data);
   
      $this->session->set_flashdata('msg', 'Advertisement Added Successfully!...');

               redirect(base_url('Admin/add_advertisement') ); 
           }
           
  }
public function view_advertisement()
  {
  $data['records'] = $this->Admin_model->selectdata('advertisement');
 // print_r($data);exit;
  $this->load->view('view_advertisement',$data);
  }
public function delete_advertisement()
  {
  
      $id=$_GET['id'];
    $this->Admin_model->deletedata('advertisement',$id);
     $this->session->set_flashdata('msg', 'Your Record has been Deleted !...');
      redirect(base_url('Admin/view_advertisement') ); 
  }
   public function edit_advertisement()
   {
   
      $id=$_GET['id'];
      
    
  $data['records'] = $this->Admin_model->get_record('advertisement as ne','ne.id="'.$id.'"');
   
     
     $this->load->view('edit_advertisement',$data);
   }
    
  

   public function update_advertisement()
   {
   
   $id=$_GET['id'];

    $this->load->library('upload');
   $config = array(
   
    'upload_path' => $_SERVER['DOCUMENT_ROOT'].'/hirajevrat/admin/uploads/add',
  
      'allowed_types' => "gif|jpg|png|jpeg",
      'file' => 'file'
    );
   $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
    if ($_FILES['file']['name']) {
    if($this->upload->do_upload('file'))
    {
      $feild=$this->upload->data();
    //$path=base_url("/uploads/".$feild['raw_name'].$feild['file_ext']);
     
       $path="/uploads/add/".$feild['raw_name'].$feild['file_ext'];
   //echo $path;exit;
        $data['image'] = $path;
    }
    else
    {   
             $error = array('error' => $this->upload->display_errors());
           // print_r($error);exit;
             $msg = "Please Select Media image in gif|jpg|png|jpeg Format ";
             $this->session->set_flashdata('msg', "$msg");

               redirect(base_url('Admin/view_advertisement') ); 
    }
    }

        $data['title'] = $_POST['title'];
        $data['description'] = $_POST['description'];
              
      $this->Admin_model->updatedata('advertisement',$data,$id);
      //print_r($this->db->last_query());exit;
       $this->session->set_flashdata('msg', 'Data Updated Successfully !...');
    
  redirect(base_url('Admin/view_advertisement') ); 
  
}

public function view_product_enquiry(){
    
    $data['records']=$this->Admin_model->join2tableRecords1('product_enquiry as dr','product as sp','sp.id,sp.product_name,dr.*','sp.id=dr.product_id');
   
    //$data['records'] = $this->db->get('product_enquiry')->result_array();
   // $data['records1'] = $this->db->get_where('product',array('id'=>$data['records']['id']))->row_array();
    $this->load->view('product_enquiry',$data);
    
}

public function product_enquiry(){
    
    $data['records']=$this->Admin_model->join2tableRecords1('enquiry as dr','product as sp','sp.id,sp.product_name,dr.*','sp.id=dr.product_id');
   
    //$data['records'] = $this->db->get('product_enquiry')->result_array();
   // $data['records1'] = $this->db->get_where('product',array('id'=>$data['records']['id']))->row_array();
    $this->load->view('enquiry',$data);
    
}

public function delete_enquiry()
  {
  
      $id=$_GET['id'];
    $this->Admin_model->deletedata('enquiry',$id);
     $this->session->set_flashdata('msg', 'Your Record has been Deleted !...');
      redirect(base_url('Admin/product_enquiry') ); 
  }




public function delete_product_enquiry()
  {
  
      $id=$_GET['id'];
    $this->Admin_model->deletedata('product_enquiry',$id);
     $this->session->set_flashdata('msg', 'Your Record has been Deleted !...');
      redirect(base_url('Admin/view_product_enquiry') ); 
  }
  
  
  
  
  
  /****************Product Category***********************/



public function add_category()
  {
    //echo "hiii";
    $this->load->view('category');
  }
  public function insert_category()
 {
    if(isset($_SESSION['login']) && $_SESSION['login'] == true)
    {
      
       $this->load->library('upload');
   $config = array(
    // 'base_url' => 'http://'.$_SERVER['SERVER_NAME'].'/Admin/uploads/',
    'upload_path' => $_SERVER['DOCUMENT_ROOT'].'/hirajevrat/admin/uploads/',
    // 'upload_path' => './Admin/uploads/ ',
      'allowed_types' => "gif|jpg|png|jpeg",
      'file' => 'file',
     
    );
//print_r($config);exit;
   $this->load->library('upload', $config);
        $this->upload->initialize($config);
    if($this->upload->do_upload('file'))
    {
      $feild=$this->upload->data();
   //  $path=base_url("/uploads/".$feild['raw_name'].$feild['file_ext']);
    // echo $path;exit;
       $path="/uploads/".$feild['raw_name'].$feild['file_ext'];
        $path1="/uploads/".$feild['raw_name'].$feild['file_ext'];
     
   //echo $path;exit;
    }
    else
    {   
             $error = array('error' => $this->upload->display_errors());
           // print_r($error);exit;
             $msg = "Please Select Media image in gif|jpg|png|jpeg Format ";
             $this->session->set_flashdata('msg', "$msg");

               redirect(base_url('Admin/add_category') ); 
    }
    
    
               $data = array(
              'category' => $_POST['category'],
                'slug' => str_replace(" ", "-", $_POST['slug']),
               
                'image' =>$path,
                'feature_image' =>$path1
                 
               );
          
       
      $this->Admin_model->insertdata('primium_category',$data);
      $this->session->set_flashdata('msg', 'Category Added Successfully!...');

              redirect(base_url('Admin/add_category') );  
        
        
        
           }
      
 else{
      redirect(base_url());
    }

    
  }  
public function view_category()
  {
  $data['records'] = $this->Admin_model->selectdata('primium_category');
 // print_r($data);exit;
  $this->load->view('view_category',$data);
  }
public function delete_category()
  {
  
      $id=$_GET['id'];
    $this->Admin_model->deletedata('primium_category',$id);
     $this->session->set_flashdata('msg', 'Your Record has been Deleted !...');
      redirect(base_url('Admin/view_category') ); 
  }
   public function edit_category()
   {
   
      $id=$_GET['id'];
      $data['records'] = $this->Admin_model->get_record('primium_category as ne','ne.id="'.$id.'"');
      $this->load->view('edit_category',$data);
   }
    
   public function update_category()
   {
    if(isset($_SESSION['login']) && $_SESSION['login'] == true)
    {
      $id=$_GET['id'];
      
        $this->load->library('upload');
   $config = array(
    // 'base_url' => 'http://'.$_SERVER['SERVER_NAME'].'/Admin/uploads/',
    'upload_path' => $_SERVER['DOCUMENT_ROOT'].'/hirajevrat/admin/uploads/',
    // 'upload_path' => './Admin/uploads/ ',
      'allowed_types' => "gif|jpg|png|jpeg",
      'file' => 'file',
      
    );
//print_r($config);exit;
   $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
         if ($_FILES['file']['name']) {
    if($this->upload->do_upload('file'))
    {
      $feild=$this->upload->data();
    //$path=base_url("/uploads/".$feild['raw_name'].$feild['file_ext']);
     
       $path="/uploads/".$feild['raw_name'].$feild['file_ext'];
   //echo $path;exit;
        $data['image'] = $path;
    }
         
  
    else
    {   
             $error = array('error' => $this->upload->display_errors());
           // print_r($error);exit;
             $msg = "Please Select Media image in gif|jpg|png|jpeg Format ";
             $this->session->set_flashdata('msg', "$msg");

               redirect(base_url('Admin/view_category') ); 
    }
         }
   
      $data['category'] = $_POST['category'];
      $data['slug'] = str_replace(" ", "-", $_POST['slug']);
      
    //  print_r($data['slug']);exit;
      
      
   
          
     $this->Admin_model->updatedata('primium_category',$data,$id);
       $this->session->set_flashdata('msg', 'Data Updated Successfully !...');
    
  redirect(base_url('Admin/view_category') ); 
      
   }
  else{
      redirect(base_url());
    }
    }
    
    
    public function export_csvenquiry(){ 
    // file name 
    
    
    
    $filename = 'enquiry_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
    
     $all_product_ = $this->db->get('product')->result_array();
   foreach($all_product_ as $value){
       $all_product[$value['id']]=$value;
    }
    $data['all_product'] = $all_product;

  $result = $this->db->get('product_enquiry')->result_array();
    $new_arr = array();
    foreach($result as $value){
       
          $value['product_id'] = $all_product[$value['product_id']]['product_name'];
        
        
        
        $new_arr[] = $value;
    }

 
   
    $file = fopen('php://output','w');
      $header = array("Sr No","Product Name","Full Name","Mobile No","Estimate Quantity","Unit Type","Order Value","Reselling","Requirement Details","Currency","Enquiry Date"); 
    fputcsv($file, $header);
    
    //print_r($result);exit;
    foreach ($new_arr as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
  }


 public function export_csvcategory(){ 
    // file name 
    
    
    
    $filename = 'category_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
    
    /* $all_product_ = $this->db->get('product')->result_array();
   foreach($all_product_ as $value){
       $all_product[$value['id']]=$value;
    }
    $data['all_product'] = $all_product;*/

  $result = $this->db->select('id,category,image,feature_image')->get('category')->result_array();
  /*  $new_arr = array();
    foreach($result as $value){
       
          $value['product_id'] = $all_product[$value['product_id']]['product_name'];
        
        
        
        $new_arr[] = $value;
    }*/

 
   
    $file = fopen('php://output','w');
      $header = array("Sr No","Category Name","Image","Feature Image"); 
    fputcsv($file, $header);
    
    //print_r($result);exit;
    foreach ($result as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
  }


public function export_csvproduct(){ 
    // file name 
    
    
    
    $filename = 'product_'.date('Ymd').'.csv'; 
    header("Content-Description: File Transfer"); 
    header("Content-Disposition: attachment; filename=$filename"); 
    header("Content-Type: application/csv; ");
    
     $all_category_ = $this->db->get('category')->result_array();
   foreach($all_category_ as $value){
       $all_category[$value['id']]=$value;
    }
    $data['all_category'] = $all_category;

  $result = $this->db->select('id,category_id,product_name,price,style_no,metal,stamped,conclusion,shape,color,clarity,weight,certificate_no,comment,total_weight,short_description,description,image')->get('product')->result_array();
    $new_arr = array();
    foreach($result as $value){
       $value['image']=basename($value['image']);
          $value['category_id'] = $all_category[$value['category_id']]['category'];
        
        
        
        $new_arr[] = $value;
    }

 
   
    $file = fopen('php://output','w');
      $header = array("Sr No","Category Name","Product Name","Price","style no","metal","stamped","conclusion","shape","color","clarity","weight","certificate no","comment","total_weight","short description","description","Image"); 
    fputcsv($file, $header);
    
    //print_r($result);exit;
    foreach ($new_arr as $key=>$line){ 
      fputcsv($file,$line); 
    }
    fclose($file); 
    exit; 
  }


public function product_zip(){
    
    $files1 = $this->db->select('image')->get('product')->result_array();
    $files2 = $this->db->select('product_iamge as image')->get('product_image')->result_array();
    
    $files=array_merge($files1,$files2);
  //  echo "<pre>";
  //  print_r($files);exit;

# create new zip object
$zip = new ZipArchive();

# create a temp file & open it
$tmp_file = tempnam('.', '');
$zip->open($tmp_file, ZipArchive::CREATE);

# loop through each file
foreach ($files as $file) {
    # download file
    $download_file = file_get_contents($file['image']);

    #add it to the zip
    $zip->addFromString(basename($file['image']), $download_file);
}

# close zip
$zip->close();

# send the file to the browser as a download
header('Content-disposition: attachment; filename="my file.zip"');
header('Content-type: application/zip');
readfile($tmp_file);
unlink($tmp_file);
    
    
    
}


  public function delete_image()
{
  
      $id=$_GET['id'];
    $this->Admin_model->deletedata('product_image',$id);
     $this->session->set_flashdata('msg', 'Your Image has been Deleted !...');
      redirect(base_url('Admin/view_product')); 
  
}

public function update_image()
{
  
      $id=$_GET['id'];
  
      $this->Admin_model->updatedata("product_image", $data,$id);
     $this->session->set_flashdata('msg', 'Your Image has been Updated !...');
   redirect(base_url('Admin/view_product')); 
  
}



}


